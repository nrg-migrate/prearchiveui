qx.Class.define("prearchiveui.UserNotification", 
{
  extend : qx.core.Object,
  type : "singleton",
  construct : function () {
    this.initNotification();
  },
  properties : {
    notificationArea : {
      check : "qx.ui.basic.Atom",
      deferredInit : true
    },
    notificationContainer : {
      check : "qx.ui.root.Inline",
      deferredInit : true
    }
  },
  members : {
    initNotification : function () {
	this.initNotificationArea(new qx.ui.basic.Atom(""));
	this.initNotificationContainer(new qx.ui.root.Inline(document.getElementById("col_table")));
	this.addNotification();
    },
    updateNotification : function (str) {
      if (this.getNotificationContainer().getVisibility() == "hidden") {
	this.getNotificationContainer().setVisibility("visible");
      }
      this.getNotificationArea().setLabel(str);
    },
    show : function () {
      this.getNotificationContainer().setVisibility("visible");
    },
    hide : function () {
      this.getNotificationContainer().setVisibility("hidden");
    },
    addNotification : function () {
      this.getNotificationContainer().add(this.getNotificationArea());
    }// ,
    // removeNotification : function () {
    //   //this.getNotificationArea().getLayoutParent().remove(this.getNotificationArea());
    //   //this.getNotificationArea().dispose();
    // }
  }
});
