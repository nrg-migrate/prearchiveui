/**
 * <pre>
 * Manage the session table UI. This class should only be instantiated once by the controller.
 *
 * This class is responsible managing various views of the prearchive session table.
 * </pre>
 */
qx.Class.define("prearchiveui.SessionTableView",
{
  extend : qx.core.Object,
  construct : function () {
    this.base(arguments);
  },
  statics : {
    /**
     * View that shows all sessions
     */
    ALL      : 0,
    /**
     * View that shows only sessions selected for bulk actions
     */
    SELECTED : 1,
    /**
     * View that shows only sessions that match the search criteria
     */
    FILTERED : 2
  },
  properties : {
    /**
     * The session table
     */
    table : {
      check : "qx.ui.table.Table"
    }
  },
  /**
   *
   */
  events : {
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeSessionSelection" : "qx.event.type.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "created" : "qx.event.type.Event",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeSelect" : "qx.event.type.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeUnselect" : "qx.event.type.Data"
  },
  members : {
    __created : false,
    __views : {
      selected : {},
      filtered : {}
    },
    __filteredIds : {},
    /**
     * Instantiate the table model
     */
    __initTableModel : function () {
      var model = new prearchiveui.SmartTableUniqueId();
          return model;
    },

    /**
     * Updates the FILTERED view with a new set of indices.
     * @param ids {Array} : list of unique indices
     */
    setFiltered : function (ids){
      this.__filteredIds = this.__makeFilteredIds(ids);
      this.updateFilteredView();
    },

    /**
     * Takes a list of unique session ids and returns an object where each id is a key and its value is 0.
     * This way when the filtered view is refreshed for each session 's' it only has to check whether the output objects' s
     * property is null or not. Check '__addFilteredView' to see how this object is used.
     *
     * This is purely an efficiency hack to get around limitations of IE 8 and earlier .
     *
     * @param ids {Array} : Array of integers representing unique ids of sessions that belong in the filtered view
     */
    __makeFilteredIds : function (ids) {
      var ret = {};
      if (ids.length > 0) {
	for (var i =0 ; i < ids.length; i++){
	  ret[ids[i]] = 0;
	}
      }
      return ret;
    },

    /**
     * An view that shows only the selected rows
     * @param model {smart.table.Default} : SmartTable model on which to create a new view
     */
    __addSelectedView : function (model) {
      this.__views.selected = model.newView(function (r) {
					      return r[0];
					    });
    },

    /**
     * Return a unique session ids of the rows that are checked.
     */
    getChecked : function () {
      return this.__getTableModel().getRowArray().filter(this.__isChecked).map(this.__getUniqueId);
    },

    /**
     * A view showing only the filtered rows
     * @param model {smart.table.Default} : SmartTable model on which to create a new view
     */
    __addFilteredView : function (model) {
      var that = this;
      this.__views.filtered = model.newView(function (r) {
					      // See the header comment for the  __makeFilteredIds function
                                              // for an explanation of why __filteredIds is an object and not an array.
					      return that.__filteredIds[that.__getUniqueId(r)] != null;
					    });
    },

    /**
     * <pre>
     * Listens for:
     * 1. Change in row selection and in turn fires the "changeSessionSelection" event with the selected sessions
     * </pre>
     * @param table {qx.ui.table.Table} : table to which to add listeners
     */
    __addTableListeners : function (table) {
      var that = this;
      table.getSelectionModel().addListener("changeSelection", function (data) {
						var selection = [];
						table.getSelectionModel().iterateSelection(function (ind) {
                                                                                           var _sel = (that.__getUniqueId(table.getTableModel().getRowData(ind)));
											   selection.push(_sel);
                                                                                         });
						that.fireDataEvent("changeSessionSelection" , selection);
                                            }, this);
      // Single clicking the check box column selects it
      table.addListener("cellClick", function(e) {
			  var column = e.getColumn();
			  if (column == 0) {
			    this.toggleSelection();
			  }
			},this);
      // double clicking anywhere else on the row selects it
      table.addListener("cellDblclick", function (e){
			  this.toggleSelection();
			},this);
    },

	/**
     * Refresh the table.
     * @param controller {prearchiveui.SessionsController} : allows access to the blocker which blocks the application while this operation is running. TODO : make this a function instead of passing in the object.
     */
    refreshAll : function (controller)  {
	  controller.__sessions = new prearchiveui.Sessions("/REST/prearchive", "csv");
	  controller.__sessions.addListener("loaded", function (e){
	    controller.__rootContainer = new qx.ui.root.Inline(document.getElementById("prearchive_table"));
        controller.__rootContainer.setZIndex(0);
        controller.__initProject = controller.__lookForInitialProject();
        controller.__detailsPane = new qx.ui.container.Composite(new qx.ui.layout.VBox());
        controller.__container = new qx.ui.container.Composite(new qx.ui.layout.HBox());
        controller.__rootContainer.add(controller.__container);
	    controller.__initViews();
      },controller);
	  
	  this.__views = [];
      controller.__tableContainer = new qx.ui.groupbox.GroupBox("Sessions");
      controller.__tableContainer.setLayout(new qx.ui.layout.VBox());
    },
	
    /**
     * Unselect all selected rows by unchecking the checkbox in the corresponding row in view 0
     * and letting the change propogate to the current view.
     * @param controller {prearchiveui.SessionsController} : allows access to the blocker which blocks the application while this operation is running. TODO : make this a function instead of passing in the object.
     */
    unselectAll : function (controller)  {
      var that = this;
      var context = {
          model : this.__getTableModel(),
          current : 0
      };
      var numRows = this.__getTableModel().getRowCount(0);
      var func = function (context) {
	if (context.current < numRows) {
	  context.model.setValueNoUpdate(0,context.current,false,0);
	  context.current++;
	}
      };
      this.doProgressive(func, numRows, 200, context,controller);
    },
	
	 /**
     * Select all rows by checking the checkbox in the corresponding row in view 0
     * and letting the change propogate to the current view.
     * @param controller {prearchiveui.SessionsController} : allows access to the blocker which blocks the application while this operation is running. TODO : make this a function instead of passing in the object.
     */
    selectAll : function (controller)  {
      var that = this;
      var context = {
          model : this.__getTableModel(),
          current : 0
      };
      var numRows = this.__getTableModel().getRowCount(0);
      var func = function (context) {
	if (context.current < numRows) {
	  context.model.setValueNoUpdate(0,context.current,true,0);
	  context.current++;
	}
      };
      this.doProgressive(func, numRows, 200, context,controller);
    },

    /**
     * Return the list of row indices selected in this view.
     */
    selectionIds : function () {
	var ranges = this.__getSelectionModel().getSelectedRanges();
	var indices = [];
	for (var i = 0; i < ranges.length ; i++){
	  for (var j = ranges[i].minIndex; j <= ranges[i].maxIndex; j++) {
	    indices.push(j);
	  }
	}
	return indices;
    },

    /**
     * Takes an array of row indices and outputs an array of the corresponding unique row indices
     * @param ids {Array} : unique session ids
     */
    uniqueIds : function (ids) {
      return ids.map(function (ind) {
		       return this.__getTableModel().getRowId(this.__getTableModel().getRowReference(ind));
		     }, this);
    },

    /**

     * Takes an array of row indices from the present view and returns to an array of the corresponding
     * row indices in view 0.
     * @param ids {Array} : unique session ids
     */
    defaultViewIds : function (ids) {
      return this.uniqueIds(ids).map(function(id) {
		       return this.__getTableModel().getRowIndexById(0,id);
		     },this);
    },

    /**
     * Checks the check box column in the rows selected
     * @param controller {prearchiveui.SessionsController} : allows access to the blocker which blocks the application while this operation is running. TODO : make this a function instead of passing in the object.
     */
    selectSelection : function (controller) {
      this.setCheckBoxColumn(true,controller);
    },

    /**
     * Unchecks the check box column in the rows selected
     * @param controller {prearchiveui.SessionsController} : allows access to the blocker which blocks the application while this operation is running. TODO : make this a function instead of passing in the object.
     */
    unselectSelection : function(controller) {
      this.setCheckBoxColumn(false,controller);
    },

    /**
     * Utility method that checks or unchecks the check box column in the rows selected.
     * @param val {Boolean} : value that the check box needs to be set to
     * @param controller {prearchiveui.SessionsController} : allows access to the blocker which blocks the application while this operation is running. TODO : make this a function instead of passing in the object.
     */
    setCheckBoxColumn : function (val,controller) {
      var that = this;
      var ids = this.defaultViewIds(this.selectionIds());
      if (controller) {
	var context = {
          model : this.__getTableModel(),
          current : 0
	};
	var func = function (context) {
	  context.model.setValueNoUpdate(0,ids[context.current],val,0);
	  context.current++;
	};

	this.doProgressive(func, ids.length, 200, context,controller);
      }
      else {
	this.__getTableModel().setValueNoUpdate(0,ids[0],val,0);
	that.__getTableModel().__evalAllFilters(false,true);
	this.__getTableModel().forceRedraw();
      }
    },

    /**
     *
     * Create and render a progress bar for the given table operation. For more on
     * how the progress bar works see the documentation for prearchiveui.Utils.addProgressive.
     *
     * After the progress bar is done rendering the entire table is redrawn and changes
     * the selected rows are propagated through the views.
     *
     * @param func {Function} function to be performed
     * @param reps {Int} # of times to do 'func'
     * @param batch {Int} chunks of 'func' - great name for a band.
     * @param context {Object} persistent state between invocations of 'func
     * @param controller {prearchiveui.SessionsController} controller to block while
     */
    doProgressive : function (func,
                              reps,
			      batch,
			      context,
			      controller)
    {
      var that = this;
      var block = qx.lang.Function.bind(controller.block,controller);
      // Re-render the table.
      var redraw = function () {
	that.__getTableModel().__evalAllFilters(false,true);
	that.__getTableModel().forceRedraw();
	controller.unblock();
      };
      prearchiveui.Utils.addProgressive(func,
	                                reps,
					batch,
					this.getTable().getLayoutParent(),
					context,
                                        block,
					redraw);
    },

    /**
     * Change the project value of the given session.
     * @param unique_id {Number} Unique id of the session
     * @param proj {String} New project name
     */
    setProject : function (unique_id, proj) {

    },

    /**
     * Instantiate a table with the given model and add columns, listeners, selection options
     * and the selection default view.
     * NOTE (2/21/2011) : Sorting on the check box column been turned off because it introduced
     * a bug. To reproduce the bug sort on the check box column and check or uncheck the boxes andn
     * rows that are not checked become checked/unchecked. We will deal with this if
     * it is a requested feature.

     * @param model {qx.ui.table.Model} The table model
     */
    __init_table : function (model) {
      var columns = [" ", "Project" , "Subject", "Session", "Scan Date", "Upload Date", "Status", "Scanner"];
      model.setColumns(columns);
	  model.setCaseSensitiveSorting(false);
      model.setColumnSortable(0,false);
      var table =  new qx.ui.table.Table(model);
      table.getTableColumnModel().setColumnWidth(0,25);
      table.getTableColumnModel().setColumnWidth(1,75);
      table.getTableColumnModel().setColumnWidth(2,75);
      table.getTableColumnModel().setColumnWidth(4,100);
      table.getTableColumnModel().setColumnWidth(5,120);
      table.getTableColumnModel().setColumnWidth(6,100);
      table.getTableColumnModel().setColumnWidth(7,75);
      table.getTableColumnModel().setColumnVisible(7, false);
      var date_renderer = new qx.ui.table.cellrenderer.Date("left","black","normal","normal");
      table.getTableColumnModel().setDataCellRenderer(4, date_renderer);
      table.getTableColumnModel().setDataCellRenderer(5, date_renderer);
      table.getTableColumnModel().setDataCellRenderer(0, new qx.ui.table.cellrenderer.Boolean());
      table.setShowCellFocusIndicator(false);
      table.getTableColumnModel().setDataCellRenderer(6,new prearchiveui.StatusRenderer());
      table.setHeight(468); 
      model.setColumnEditable(0,false);
      var Model = qx.ui.table.selection.Model;
      table.getSelectionModel().setSelectionMode(Model.MULTIPLE_INTERVAL_SELECTION);
      this.__addTableListeners(table);
      this.__addSelectedView(model);
      this.__addFilteredView(model);
      return table;
    },

    /**
     * <pre>
     * Build the progress bar. In the current implementation the table does not appear on screen
     * until all the data is loaded. The advantage of this approach is that it avoids updating the table
     * which is an expensive operation and it only needs to be built once. Loading all the data into
     * the table model and rendering the data seems to be fairly fast on Chrome and Firefox and
     * moderately fast on IE. The disadvantage is that if there are network issues the user has to wait
     * until the end to see their data.
     *
     * In addition every row in the table has a unique id which is the unique id of that session
     * in the model. The unique id is tacked onto the end of each row array and can be retrieved
     * but is never rendered because there is no column for it.
     *
     * TODO: refactor to use prearchiveui.Utils.addProgressive.
     * </pre>
     * @param model {qx.ui.table.Model} The table model
     * @param data {Array} A list of sessions
     * @param progress_bar {} The progress bar to show while the table is rendering
     * @param container {} The root container which is blocked while this table is rendering
     * @param preFunc {Function} Function to run before the progress bar starts rendering
     * @param postFunc {Function} Function to run after the progress bar renders
     */
    __make_progressive_table : function (model,data,progress_bar,container, preFunc, postFunc) {
      var progressive_data_model =  new qx.ui.progressive.model.Default();
      var that = this;
      var context = {
          data : data,
          table_data_model : model,
          current : 0
      };
      progress_bar.setBatchSize(1000);
      var addFunc = function(func) {
        var ret = {
          renderer : "func",
          data     : func
        };
        return ret;
      };
      progress_bar.addListener("renderStart",function (e) {
				 preFunc();
                                 var state = e.getData().state;
                                 state.getUserData().context = context;
                               }, this);
      progress_bar.addListener("renderEnd",function(e) {
                                 that.setTable(that.__init_table(model));
                                 container.add(that.getTable());
                                 qx.event.Timer.once(
                                   function() {
                                     container.remove(this);
                                     this.dispose();
                                   },
                                   this, 0);
                                 that.__created = true;
				 postFunc();
                                 that.fireEvent("created");
                               });
      for (var i = 0; i < data.length ; i++) {
        progressive_data_model.addElement(addFunc(function (userData) {
	  var context = userData.context;
          var sessions = context.data;

		  var internalStatus = sessions[context.current].status;
		  var statusToDisplay = "Unknown Status";
		  if(internalStatus=="BUILDING" || internalStatus=="QUEUED" || internalStatus=="_QUEUED" || internalStatus=="QUEUED_BUILDING" ){
			statusToDisplay = "Build pending";
		  }
		  else if (internalStatus=="_BUILDING" ){
			statusToDisplay = "Building now";
		  }
		  else if (internalStatus=="ARCHIVING" || internalStatus == "QUEUED_ARCHIVING"){
			statusToDisplay = "Archive pending";
		  }
		  else if (internalStatus=="_ARCHIVING" ){
			statusToDisplay = "Archiving now";
		  }
		  else if (internalStatus=="MOVING" || internalStatus == "QUEUED_MOVING"){
			statusToDisplay = "Move pending";
		  }
		  else if (internalStatus=="_MOVING" ){
			statusToDisplay = "Moving now";
		  }
		  else if (internalStatus=="DELETING" || internalStatus == "QUEUED_DELETING"){
			statusToDisplay = "Delete pending";
		  }
		  else if (internalStatus=="_DELETING" ){
			statusToDisplay = "Deleting now";
		  }
		  else if (internalStatus=="RECEIVING" || internalStatus=="_RECEIVING"){
			statusToDisplay = "Receiving";
		  }
		  else if (internalStatus=="READY" ){
			statusToDisplay = "Ready";
		  }
		  else if (internalStatus=="CONFLICT" || internalStatus=="_CONFLICT"){
			statusToDisplay = "Conflict";
		  }
		  else if (internalStatus=="ERROR"){
			statusToDisplay = "Error";
		  }
	  
          var row = [false,
		     sessions[context.current].project,
		     sessions[context.current].subject,
		     sessions[context.current].name,
		     sessions[context.current].scan_date,
		     sessions[context.current].uploaded,
		     statusToDisplay,
		     sessions[context.current].scanner,
		     sessions[context.current].unique_id];
	  context.table_data_model.addRows([row]);
  	  context.current++;
        }));
      };
      progress_bar.setDataModel(progressive_data_model);
    },

    /**
     * Return the unique id, defined as the last element in a row array
     * @param row {Array} Row from which to return the unique id.
     */
    __getUniqueId : function (row) {
      return row[row.length -1];
    },

    /**
     * Return true if the row is checked, false if not
     * @param row {Object} Row that is checked.
     */
    __isChecked : function (row) {
      return row[0];
    },

    /**
     * Wrapper around getting the table model
     */
    __getTableModel : function () {
      return this.getTable().getTableModel();
    },
    /**
     * Wrapper around getting the selection model
     */
    __getSelectionModel : function () {
      return this.getTable().getSelectionModel();
    },
    /**
     * Return the row with the unique id or null.
     * @param unique_id {Integer} Unique row to search on.
     */
    __getRow : function (unique_id) {
      var numRows = this.__getTableModel().getData().length;
      for (var i = 0; i < numRows ; i++) {
        if (this.__getUniqueId(this.__getTableModel().getData()[i]) == unique_id) {
          return i;
        }
      }
      return null;
    },

    /**
     * create progress bar and table. Should only be called once and only by the controller.
     * @param dataFunc {Function} Function that produces the data that goes in the table.
     * @param container {Container} Container to block while is table is being created.
     * @param preFunc {Function} Function to run before the table is created.
     * @param postFunc {Function} Function to run after the table is created.
     */
    create : function (dataFunc, container, preFunc,postFunc) {
      var progressBar = prearchiveui.Utils.makeProgressive();
      container.add(progressBar);
      this.__make_progressive_table(this.__initTableModel(), dataFunc(), progressBar,container, preFunc, postFunc);
      progressBar.render();
    },

    /**
     * Remove a row from the table
     * @param uniqueId {Number} Unique id of row to remove
     */
    deleteRow : function (uniqueId) {
      var rowId = this.__getTableModel().getRowById(this.__getTableModel().getView(),uniqueId);
      this.__getTableModel().removeReferencedRows([rowId], false);
      this.__getTableModel().__evalAllFilters(false, true);
      this.__getTableModel()._updateAssociationMaps(0);
      this.__getTableModel().forceRedraw();
    },

    /**
     * Reset the row at the given index with new data
     * @param index {Integer} Row index
     * @param data {Object} New session data
     */
    __setRow : function (index,data) {
      this.__getTableModel.setRows([data],index);
    },


       /**
     * Update the data in an existing row
     * @param data {Object} New session data.
     */
    onUpdate : function (data) {
      // find the row in view 0
      var index = this.__getTableModel().getRowIndexById(0, data.unique_id);
      if (index != null) {
        var old = this.__getTableModel().getRowData(index,0);
		
		var internalStatus = data.status;
		  var statusToDisplay = "Unknown Status";
		  if(internalStatus=="BUILDING" || internalStatus=="QUEUED" || internalStatus=="_QUEUED" || internalStatus=="QUEUED_BUILDING" ){
			statusToDisplay = "Build pending";
		  }
		  else if (internalStatus=="_BUILDING" ){
			statusToDisplay = "Building now";
		  }
		  else if (internalStatus=="ARCHIVING" || internalStatus == "QUEUED_ARCHIVING"){
			statusToDisplay = "Archive pending";
		  }
		  else if (internalStatus=="_ARCHIVING" ){
			statusToDisplay = "Archiving now";
		  }
		  else if (internalStatus=="MOVING" || internalStatus == "QUEUED_MOVING"){
			statusToDisplay = "Move pending";
		  }
		  else if (internalStatus=="_MOVING" ){
			statusToDisplay = "Moving now";
		  }
		  else if (internalStatus=="DELETING" || internalStatus == "QUEUED_DELETING"){
			statusToDisplay = "Delete pending";
		  }
		  else if (internalStatus=="_DELETING" ){
			statusToDisplay = "Deleting now";
		  }
		  else if (internalStatus=="RECEIVING" || internalStatus=="_RECEIVING"){
			statusToDisplay = "Receiving";
		  }
		  else if (internalStatus=="READY" ){
			statusToDisplay = "Ready";
		  }
		  else if (internalStatus=="CONFLICT" || internalStatus=="_CONFLICT"){
			statusToDisplay = "Conflict";
		  }
		  else if (internalStatus=="ERROR"){
			statusToDisplay = "Error";
		  }
		
        this.__getTableModel().setRow(index,[old[0],
					     data.project,
                                             data.subject,
                                             data.name,
                                             data.scan_date,
                                             data.uploaded,
											 statusToDisplay,
					     data.unique_id],0);
	this.__getTableModel().__evalAllFilters(false,true);
	this.__getTableModel().forceRedraw();
	return true;
      }
      else {
        return false;
      }
    },

    /**
     * Toggle the check box on the selected rows.
     */
    toggleSelection : function () {
      if (this.selectionIds().length == 1) {
	var row = this.__getTableModel().getRowData(this.selectionIds()[0]);
	if (row[0]) {
	  this.unselectSelection();
	}
	else {
	  this.selectSelection();
	}
      }
    },

    /**
     * Toggle between showing the selected rows and all of them.
     */
    toggleShowSelected : function () {
      if (this.__getTableModel().getView() == prearchiveui.SessionTableView.SELECTED) {
        this.__getTableModel().setView(prearchiveui.SessionTableView.ALL);
      }
      else {
        this.__getTableModel().setView(prearchiveui.SessionTableView.SELECTED);
      }
    },
    /**
     * Toggle between showing the filtered rows and all of them.
     */
    toggleShowFiltered : function () {
      if (this.__getTableModel().getView() == prearchiveui.SessionTableView.FILTERED) {
        this.__getTableModel().setView(prearchiveui.SessionTableView.ALL);
      }
      else {
        this.__getTableModel().setView(prearchiveui.SessionTableView.FILTERED);
      }
    },
    /**
     * Re-create the filtered view
     */
    updateFilteredView : function () {
      this.__getTableModel().updateView(prearchiveui.SessionTableView.FILTERED);
    },

    /**
     * Show all the rows
     */
    showAll : function (){
      this.__getTableModel().setView(prearchiveui.SessionTableView.ALL);
    },
    /**
     * Show the filtered rows
     */
    showFiltered : function (){
      this.__getTableModel().setView(prearchiveui.SessionTableView.FILTERED);
    },
    /**
     * Show the selected rows.
     */
    showSelected : function () {
      this.__getTableModel().setView(prearchiveui.SessionTableView.SELECTED);
    }

  },

  destruct : function () {
    this.setTable(null);
  }

});
