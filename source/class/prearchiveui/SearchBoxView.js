/**
 * A view that composes that accepts and renders search widgets.
 */
qx.Class.define("prearchiveui.SearchBoxView",
{
  extend : qx.core.Object,
  /**
   * @param uploadView{prearchiveui.DataRangeView} Upload date search widget.
   * @param scanDateView{prearchiveui.DateRangeView} Scan date search widget.
   * @param projectView{prearchiveui.SearchTextfieldView} Project search widget.
   * @param statusView{prearchiveui.SearchStatusView} Status search widget.
   * @param sessionSubjectView{prearchiveui.SearchTextfieldView} Session/Subject search widget
   */
  construct : function (uploadView, scanDateView, projectView, statusView, sessionSubjectView) {
    this.base(arguments);
    this.initUploadView(uploadView);
    this.initDateView(scanDateView);
    this.initProjectView(projectView);
    this.initStatusView(statusView);
    this.initSessionSubjectView(sessionSubjectView);
  },
  events : {
    /**
     * See prearchiveui.SessionController for explanation on this event.
     */
    "clearFilters" : "qx.type.event.Data"
  },
  properties : {
    /**
     * See constructor comment for explanation on this property.
     */
    uploadView : {
      check : "prearchiveui.DateRangeView",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    dateView : {
      check : "prearchiveui.DateRangeView",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    projectView : {
      check : "prearchiveui.SearchTextfieldView",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    sessionSubjectView : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    statusView : {
      check : "prearchiveui.SearchStatusView",
      deferredInit : true
    }
  },
  members : {
    __searchBoxContainer : null,
    __buttonPane : null,
    __initButtonPane : function () {
      var paneLayout = new qx.ui.layout.HBox().set({
						     spacing: 4,
						     alignX : "right"
						   });

      this.__buttonPane = new qx.ui.container.Composite(paneLayout).set({
								       paddingTop: 11
								     });
      this.__buttonPane.add(this.__createButton("Clear"));
      this.__searchBoxContainer.add(this.__buttonPane, {row:6, column: 0, colSpan: 2});
    },
    __createButton : function (name, event, func) {
      var button = new qx.ui.form.Button(name);
      if (event != null) {
      	if (func != null) {
      	  button.addListener(event,func,this);
      	}
      }
      return button;
    },
    /**
     * Display the filter widgets
     */
    create : function () {
      var that = this;
      this.__searchBoxContainer = new qx.ui.groupbox.GroupBox("Filters");
      this.__searchBoxContainer.setLayout(new qx.ui.layout.Grid(8,8));
      var currRow = 0;
      var addFields = function (text,fields) {
	that.__searchBoxContainer.add(prearchiveui.Utils.makeRichLabel("<b>" + text + ": </b>"),
				      {row : currRow, column : 0});
	for (var i = 0; i < fields.length; i++){
	  that.__searchBoxContainer.add(fields[i], {row : currRow, column : 1});
	  currRow++;
	}
      };
      addFields("Projects",        [this.getProjectView().getTextfield()]);
      addFields("Upload Date" ,    [this.getUploadView().getFirst(),
                                    this.getUploadView().getSecond()]);
      addFields("Scan Date",       [this.getDateView().getFirst(),
			            this.getDateView().getSecond()]);
      addFields("Status",          [this.getStatusView().getSelectBox()]);
      addFields("Subject/Session", [this.getSessionSubjectView().getTextfield()]);
      return this.__searchBoxContainer;
    }
  }
});
