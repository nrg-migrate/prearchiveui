/**
 * Creates a dialog that informs the user of something
 */
qx.Class.define("prearchiveui.DialogView",
{
  extend : qx.core.Object,
  /**
   * @param title{String} The title of the dialog window.
   * @param message{String} The message in the dialog window.
   * @param okAction{Function} Run if the user confirms
   */
  construct : function (title, message) {
    this.base(arguments);
    this.setTitle(title);
    this.setMessage(message);
    this._init();
  },
  properties : {
    /**
     * See constructor comment for explanation on this property.
     */
    message : {
      check : "String"
    },
    /**
     * This dialog window
     */
    window : {
      check : "qx.ui.window.Window",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    title : {
      check : "String",
      init : ""
    },
    /**
     * The number of sessions on which this action will be performed.
     */
    numSessions : {
      check : "Number",
      deferredInit : true
    }
  },
  members : {
    _initWindow : function () {
      this.initWindow(new qx.ui.window.Window(this.getTitle()));
      this.getWindow().setModal(true);
      this.getWindow().setAllowMinimize(false);
      this.getWindow().setAllowMaximize(false);
      this.getWindow().setMovable(false);
      this.getWindow().setLayout(new qx.ui.layout.VBox());
      this.getWindow().moveTo(250,250);
    },
    _initMessage : function () {
      this.getWindow().add(new qx.ui.basic.Atom(this.getMessage()));
    },

    _init : function () {
      this._initWindow();
      this._initMessage();
      this._initButtons();
    },

    _initButtons : function () {
      var box = new qx.ui.container.Composite;
      box.setLayout(new qx.ui.layout.HBox(10, "right"));
      this.getWindow().add(box);

      var ok = new qx.ui.form.Button("Ok", "icon/16/actions/dialog-ok.png");
      this._initListeners(ok);

      box.add(ok);
    },

    _initListeners : function (ok) {
      ok.addListener("execute", function(e) {
		       this.getWindow().close();
		     },this);
    },

    /**
     * Dpen this dialog window.
     */
    open : function () {
      this.getWindow().open();
    }
  }
});
