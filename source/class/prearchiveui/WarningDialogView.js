/**
 * A confirmation dialog that pops up a warning.
 */
qx.Class.define("prearchiveui.WarningDialogView",
{
  extend : prearchiveui.ConfirmationDialogView,
  /**
   * @param title{String} The dialog window title
   * @param message{String} The dialog message
   */
  construct : function (title, message) {
    this.base(arguments,title,message);
  },
  members : {
    //Override
    _initMessage : function () {
      this.getWindow().add(new qx.ui.basic.Atom(this.getMessage(), "icon/32/status/dialog-error.png"));
    },
    //Override
    _initButtons : function () {
      var box = new qx.ui.container.Composite;
      box.setLayout(new qx.ui.layout.HBox(10, "right"));
      this.getWindow().add(box);
      var close = new qx.ui.form.Button("Close", "icon/16/actions/dialog-cancel.png");
      this._initListeners(close);
      box.add(close);
    },
    //Override
    _initListeners : function (close) {
      close.addListener("execute", function(e) {
		       this.getWindow().close();
		     },this);
    }
  }
});
