/**
 * A wrapper class around TokenField that creates either a regular
 * TokenField or a RestrictedTokenField if a cap is passed in.
 */
qx.Class.define("prearchiveui.SearchTextfieldView",
{
  extend : qx.core.Object,
  /**
   * @param hintText{String} Initial greyed-out text in the textfield
   * @param model{Object} The model which controls this view
   * @param cap{Int} The maximum number of entries to show in the popup
   */
  construct : function (hintText, model,cap) {
    this.base(arguments);
    this.initModel(model);
    this.initHintText(hintText);
    if (cap) {
      this.initTextfield(new prearchiveui.RestrictedToken(cap));
    }
    else {
      this.initTextfield(new tokenfield.Token());
      }
    this.getTextfield().setSelectionMode('multi');
    this.getTextfield().setHintText(this.getHintText());
    this.getTextfield().setMinChars(1);
  },
  properties : {
    /**
     * See constructor comment for explanation on this property.
     */
    hintText : {
      nullable : false,
      check : "String",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    textfield : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    model : {
      check : "prearchiveui.SearchTextfieldModel",
      deferredInit : true
    }
  }
});
