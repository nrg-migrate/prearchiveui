/**
 * This class abstracts away requesting information from the server and doing something with the response.
 */
qx.Class.define("prearchiveui.ServerRequest",
{
  extend : qx.core.Object,
  /**
   * @param url {String} The *relative* url of request. Mandatory argument.
   * @param cxt {Object} The context of the calling object. Defaults to "this".
   * @param format {String} The expected format of the response . A null format indicates that this request does not take a format attribute.
   * @param attributes {Map} An association list of url attributes. The default is {}.
   * @param transformer {Function} The function to which to pass the parsed response.
   *                               The default is the identity function.
   * @param method{String} The request method eg."POST"
   * @param progressive{Boolean} Whether to parse the response progressively
   * @param event {String} The event to fire when the response has been completed and transformed
   *                       The default is "loaded".
   * @param preRequestFunc {Function} Function to evaluate before the request is made. The intended use-case is for user notification. Defaults to the identity function.
   * @param preReceiveFunc {Function} Function to evaluate once the response is ready to be streamed. The intended use-case is for user notification. Defaults to the identity function.
   * @param postReceiveFunc {Function} Function to evaluate after the response has been received. The intended use-case is to remove user notification. Defaults to the identity function.
   * @param onFail {Function} The function to call if the request fails.
   *                          The default is the identity function.
   */
  construct : function (url, cxt, format, attributes, transformer, method, event, preRequestFunc, preReceiveFunc, postReceiveFunc, onFail, progressive) {
    this.base(arguments);
    this.initUrl(url);
    if (cxt == null) {
      cxt = this;
    }
    if (method != null) {
      this.setMethod(method);
    }
    else {
      this.setMethod("GET");
    }

    if (onFail != null){
      this.setOnFail(qx.lang.Function.bind(onFail,cxt));
    }
    else {
      this.setOnFail(qx.lang.Function.bind(prearchiveui.Utils.identity,cxt));
    }

    if (preReceiveFunc != null) {
      this.setPreReceiveFunc(preReceiveFunc);
    }
    else {
      this.setPreReceiveFunc(qx.lang.Function.bind(prearchiveui.Utils.identity,cxt));
    }
    if (postReceiveFunc != null) {
      this.setPostReceiveFunc(postReceiveFunc);
    }
    else {
      this.setPostReceiveFunc(qx.lang.Function.bind(prearchiveui.Utils.identity,cxt));
    }

    if (preRequestFunc != null) {
      this.setPreRequestFunc(preRequestFunc);
    }
    else {
      this.setPreRequestFunc(qx.lang.Function.bind(prearchiveui.Utils.identity,cxt));
    }


    this.setFormat(format);
    
    if (transformer != null) {
      this.setTransformer(qx.lang.Function.bind(transformer,cxt));
    }
    else {
      this.setTransformer(qx.lang.Function.bind(prearchiveui.Utils.identity,cxt));
    }
    if (event != null) {
      this.setEvent(function (){cxt.fireDataEvent(event);});
    }
    else {
      this.setEvent(function () {cxt.fireDataEvent("loaded");});
    }
    if (attributes != null) {
      this.setAttributes(attributes);
    }
    else {
      this.setAttributes({});
    }
    if (progressive != null) {
      this.progressive(progressive);
    }
    else {
      this.progressive(false);
    }

  },
  
  events : {
    "parseComplete" : "qx.event.type.Event"
  },

  properties : {
    /**
     * See constructor comment for explanation on this property.
     */
    progressive : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    method : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    attributes : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    onFail : {
      check : "Function",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    preReceiveFunc : {
      check : "Function",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    postReceiveFunc : {
      check : "Function",
      deferredInit : true
    },

    /**
     * See constructor comment for explanation on this property.
     */
    preRequestFunc : {
      check : "Function",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    event : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    parser : {
      deferredInit : true,
      nullable : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    format : {
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    url : {
      check : "String",
      nullable : false,
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    transformer : {
      check : "Function",
      deferredInit : true
    }
  },
  members : {
    progressive : function (bool) {
      this.setProgressive(bool);
      if (this.getProgressive()) {
	this.setParser(prearchiveui.ServerRequest.formatParserMap(this.getFormat()+"-progressive"));      
      }
      else {
	this.setParser(prearchiveui.ServerRequest.formatParserMap(this.getFormat()));      
      }
    },
    /**
     * Make the request.
     */
    request : function () {
      var format = this.getFormat();
      var transform = this.getTransformer();
      var event = this.getEvent();
      var onFail = this.getOnFail();
      var method = this.getMethod();
      var preReceive = this.getPreReceiveFunc();
      var postReceive = this.getPostReceiveFunc();
      var preRequest = this.getPreRequestFunc();
      var url = prearchiveui.Utils.SERVER_ROOT + this.getUrl();
      var req = new qx.io.remote.Request(url, method);
      var parser = this.getParser();
      // a format attribute is always part of the URL and not in the POST message.
      if (parser.format != null) {
	req.setParameter("format",format, false);
      }
      req.setParameter("XNAT_CSRF", window.csrfToken);
      req = prearchiveui.ServerRequest.setAttributes(req, this.getAttributes(), method == "POST");
      req.setTimeout(100000);
      req.setParseJson(false);
      req.addListener("receiving", function () {
			preReceive();
		      },this);
      req.addListener("completed", function (evt) {
			postReceive();
			if (parser.progressive) {
			  parser.parse(evt.getContent(),function (data) {
					 transform(data);
					 event();
				       });
			}
			else {
			  var data = parser.parse(evt.getContent());
			  transform(data);
			  event();			  
			}
		      },this);
      req.addListener("failed", function(evt) {
			onFail();
		      },this);
      preRequest();
      req.send();
    }
  },
  statics : {
    /**
     * Set the attributes of a request.
     * @param req{qx.io.remote.Request} Request on which to set the attributes
     * @param attrs{Map} A map of attributes
     * @param asData{Boolean} If true, attribute is added as part of the POST message body, if false it is added to
     * url header.
     */
    setAttributes : function (req, attrs,asData){
      var attr;
      for (attr in attrs) {
	req.setParameter(attr, attrs[attr], asData);
      }
      return req;
    },

    /**
     * Map a string to a parser.
     * @param str{String} A parser name, either "json" or "csv"
     */
    formatParserMap : function (str) {
      if (str == "json") {
	return this.JSON_PARSER;
      }
      else if (str == "csv") {
	return this.CSV_PARSER;
      }
      else if (str == "csv-progressive") {
	return this.CSV_PARSER_PROGRESSIVE;
      }
      else if (str == "single") {
	return this.SINGLE_SCAN_PARSER;
      }
      else if (str == null) {
	return this.DUMMY_PARSER;
      }
      else {
	return null;
      }
    },
    /**
     * A dummy parser
     */
    DUMMY_PARSER : {
      progressive : false,
      parse : function (data) {
	return prearchiveui.Utils.identity(data);
      },
      format : null
    },
      /**
     * Parse a single response.
     */
    SINGLE_SCAN_PARSER : {
      progressive : false,
      parse : function (data) {
        return prearchiveui.Utils.removePrefix(data, "/data");
      },
      format : "single"
    },
    /**
     * Parse a JSON response
     */
    JSON_PARSER : {
      progressive : false,
      parse : function (data) {
	var json;
	// The JSON parser keeps throwing weird errors for valid JSON
	// we need this try/catch to continue execution
	try {
	  json = qx.lang.Json.parse(data);
	}
	catch (err){}
	// return the list of sessions
	return json.ResultSet.Result;
      },
      format : "json"
    },
      
    /**
     * Parse a CSV response showing progress along the way 
     * so the browser doesn't lock up for large datasets
     */
    CSV_PARSER_PROGRESSIVE: {
      progressive : true,
      format : 'csv-progressive',
      parse : function (data,afterFunc) {
	var lines = [];
	var progFuncs = new csvparser.CSVParser().csvToArrayProgressive(lines,data);
	var notification = prearchiveui.UserNotification.getInstance();
	var progressiveContainer = new qx.ui.root.Inline(document.getElementById("progress-bar"));
	var updateNotification = function () {
	  prearchiveui.Utils.notify("Read " + lines.length + " rows.");
	};

	prearchiveui.Utils.addUnboundedProgressive(progFuncs.func,
						   progFuncs.continueCase,
						   progressiveContainer, "hidden",
						   progFuncs.context,
						   function() {
						     prearchiveui.Utils.showNotification();
						   },
						   function() {
						     prearchiveui.Utils.hideNotification();
						     var tmp = prearchiveui.ServerRequest.CSV_PARSER.toObject(lines);
						     afterFunc(tmp);
						   },
						   updateNotification
						   );
      }
    },

    /**
     * Parse a CSV response.
     * It produces JSON objects where the column headers are keys and the values
     * are the appropriate row indices.
     */
    CSV_PARSER : {
      progressive : false,
      unQuote : function (str) {
	  var ret = str;
	  if (qx.lang.String.startsWith(str,"\"") && qx.lang.String.endsWith(str,"\"")){
	    ret = str.substring(1, str.length -1);
	  }
	  return ret;
      },

      toObject : function (lines) {
	// a map of column names to their column numbers
	var colName = {};
	// return list of sessions
	var ret = [];
	for (var i = 0; i < lines.length ; i++) {
	  var line = lines[i];
	  // the first line contains the column names
	  if (i == 0) {
	    for (var j = 0; j < line.length; j++) {
	      if (line[j] != undefined) {
	      	colName[j.toString()] = prearchiveui.ServerRequest.CSV_PARSER.unQuote(line[j]);
	      }
	    }
	  }
	  // create a session object and add it to the return list.
	  else {
	    var obj = {};
	    for (var j = 0; j < line.length; j++) {
	      if (line[j] != undefined) {
		obj[colName[j]] = prearchiveui.ServerRequest.CSV_PARSER.unQuote(line[j]);
	      }
	    }
	    ret.push(obj);
	  }
	}
	return ret;
      },
	
      parse : function (data) {
	var lines = new csvparser.CSVParser().csvToArray(data);
	return prearchiveui.ServerRequest.CSV_PARSER.toObject(lines);
      },
      format : "csv"
    }
  }
});

