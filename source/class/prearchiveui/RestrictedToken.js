/**
 * An extension of TokenField that restricts the number of items that can appear in the drop-down list.
 */
qx.Class.define("prearchiveui.RestrictedToken",
{
  extend : tokenfield.Token,
  /**
   * @param cap{Int} The maximum number of results to show in the popup.
   */
  construct : function (cap) {
    this.base(arguments);
    if (cap != null) {
      this.setCap(cap);
    }
  },
  properties : {
    /**
     * Message to display if there are too many results.
     */
    tooManyResultsText : {
      check : "String",
      nullable : true,
      init : "Too many matches, please continue typing ..."
    },
    /**
     * See constructor comment for explanation
     */
    cap : {
      check : "Number",
      init : 10000
    }
  },
  members : {
    // override
    populateList : function (str, data) {
      var list = this.getChildControl('list');
      if (data.length > this.getCap()) {
	this._dummy.setLabel( this.getTooManyResultsText() );
        list.add( this._dummy );
        return;
      }
      else {
	var result = qx.data.marshal.Json.createModel(data);
	if ( result.getLength() == 0 ) {
          this._dummy.setLabel( this.getNoResultsText() );
          list.add( this._dummy );
          return;
	}
	for (var i=0; i<result.getLength(); i++) {
          var label = result.getItem(i).get(this.getLabelPath());
          var item = new qx.ui.form.ListItem( this.highlight(label, str) );
          item.setModel(result.getItem(i));
          item.setRich(true);
          list.add(item);
	}
      }
    },

    /**
     * Add a token to the textfield manually. Events are fired as though
     * the user selected it from the drop-down list.
     * @param str{String} The value of the token
     */
    selectItem : function (str) {
      var item = new qx.ui.form.ListItem(str);
      item.setModel(qx.data.marshal.Json.createModel({label: str}));
      item.setRich(true);
      this._selectItem(item);
    }
  }
});
