
/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/* ************************************************************************

#asset(prearchiveui/*)

************************************************************************ */

/**
 * Main entry point of application
 */
qx.Class.define("prearchiveui.Application", {
    extend : qx.application.Inline,
    members : {
       main : function() {
           this.base(arguments);
           // Enable logging in debug variant
           //if (qx.core.Variant.isSet("qx.debug", "on")) {
	 if (qx.core.Environment.get('qx.debug') == "on") {
               // support native logging capabilities, e.g. Firebug for Firefox
               qx.log.appender.Native;
               // support additional cross-browser console. Press F7 to toggle visibility
               qx.log.appender.Console;
	   }
      // var testLines = "test\ntest2\r\ntest3\n";
      // var testFields = "\"test\",1234,,\"test,\"\"\"";
      // var testCsv = "test\",1234\n,\r\n,\"test,\"\"\"";
      // this.debug(prearchiveui.CSVParser.lines(testLines,""));
      // this.debug(prearchiveui.CSVParser.fields(testFields));
      // this.debug(prearchiveui.CSVParser.csvToArray(testCsv));
           var prearchive_table = new qx.ui.root.Inline(document.getElementById("prearchive_table"));
           var col_table = new qx.ui.root.Inline(document.getElementById("col_table"));
	   var serverRoot = document.getElementById("serverRoot").title;
	   var controller = new prearchiveui.SessionsController(prearchive_table, serverRoot);
       }
    }
});
