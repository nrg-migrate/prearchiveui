/**
 * Class that manages a list of the current selections in the prearchiveui filters
 */
qx.Class.define("prearchiveui.SearchBoxModel",
{
  extend : qx.core.Object,
  events : {
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeProjectSelection" : "qx.type.event.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeSubjectSessionSelection" : "qx.type.event.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeUploadDateSelection" : "qx.type.event.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeSessionDateSelection" : "qx.type.event.Data"
  },
  members : {
    __projects      : [],
    __subjects      : [],
    __view          : null,
    __upload_date   : new prearchiveui.DateRange(),
    __session_date  : new prearchiveui.DateRange(),

    /**
     * Add a project to project selection
     * @param proj{String} Project to add
     */
    addProject : function (proj) {
      this.__projects.push(proj);
      this.fireDataEvent("changeProjectSelection", this.__projects);
    },
    /**
     * Remove a project from the current selection
     * @param proj{String} Project to remove.
     */
    removeProject : function (proj) {
      qx.lang.Array.exclude(this.__projects, [proj]);
      this.fireDataEvent("changeProjectSelection", this.__projects);
    },
    /**
     * Add a subject/session to the current selection
     * @param subj{String} Subject or session to add.
     */
    addSubjectOrSession : function (subj)  {
      this.__subjects.push(subj);
      this.fireDataEvent("changeSubjectSessionSelection", this.__subjects);
    },
    /**
     * Remove a subject or session from the current selection
     * @param subj{String} Subject or session to remove
     */
    removeSubjectOrSession : function (subj) {
      qx.lang.Array.exclude(this.__subjects, [subj]);
      this.fireDataEvent("changeSubjectSessionSelection", this.__subjects);
    },

    /**
     * Add a upload date range to the current selection
     * @param first{Date} The start of the date range.
     * @param date{Date} The end of the date range.
     */
    addUploadDate   : function (first, date) {
      this.__upload_date.addDate(first,date);
      if (this.__upload_date.getValid()) {
	this.fireDataEvent("changeUploadDateSelection", this.__upload_date);
      }
    },
    /**
     * Add a scan date range to the current selection
     * @param first{Date} The start of the date range.
     * @param date{Date} The end of the date range.
     */
    addSessionDate  : function (first, date) {
      this.__session_date.addDate(first,date);
      if (this.__session_date.getValid()) {
	this.fireDataEvent("changeSessionDateSelection", this.__session_date);
      }
    }
  }
});
