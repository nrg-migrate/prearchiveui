/**
 * Creates a dialog that confirms some action
 */
qx.Class.define("prearchiveui.ArchiveDialogView",
{
  extend : qx.core.Object,
  /**
   * @param title{String} The title of the dialog window.
   * @param message{String} The message in the dialog window.
   * @param revAction{Function} Run if the user chooses to review and archive.
   * @param arcAction{Function} Run if the user chooses to archive.
   */
  construct : function (title, message, revAction, arcAction) {
    this.base(arguments);
    this.setTitle(title);
    this.setMessage(message);
    this.initRevAction(revAction != null ? revAction : prearchiveui.Utils.dummy);
    this.initArcAction(arcAction != null ? arcAction : prearchiveui.Utils.dummy);
    this._init();
  },
  properties : {
    /**
     * See constructor comment for explanation on this property.
     */
    message : {
      check : "String"
    },
    /**
     * This dialog window
     */
    window : {
      check : "qx.ui.window.Window",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    title : {
      check : "String",
      init : ""
    },
    /**
     * The number of sessions on which this action will be performed.
     */
    numSessions : {
      check : "Number",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    revAction : {
      check : "Function",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    arcAction : {
      check : "Function",
      deferredInit : true
    }
  },
  members : {
    _initWindow : function () {
      this.initWindow(new qx.ui.window.Window(this.getTitle()));
      this.getWindow().setModal(true);
      this.getWindow().setAllowMinimize(false);
      this.getWindow().setAllowMaximize(false);
      this.getWindow().setMovable(false);
      this.getWindow().setLayout(new qx.ui.layout.VBox());
      this.getWindow().moveTo(250,250);
    },
    _initMessage : function () {
      this.getWindow().add(new qx.ui.basic.Atom(this.getMessage()));
    },

    _init : function () {
      this._initWindow();
      this._initMessage();
      this._initButtons();
    },

    _initButtons : function () {
      var box = new qx.ui.container.Composite;
      box.setLayout(new qx.ui.layout.HBox(10, "right"));
      this.getWindow().add(box);

      var rev = new qx.ui.form.Button("Review and Archive");
      var arc = new qx.ui.form.Button("Archive");
      this._initListeners(rev,arc);

      box.add(rev);
      box.add(arc);
    },

    _initListeners : function (rev,arc) {
      rev.addListener("execute", function(e) {
		       this.getWindow().close();
		       this.getRevAction()();
		     },this);
      arc.addListener("execute", function(e) {
			   this.getWindow().close();
			   this.getArcAction()();
			 },this);
    },

    /**
     * Dpen this dialog window.
     */
    open : function () {
      this.getWindow().open();
    }
  }
});
