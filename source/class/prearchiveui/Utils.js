/**
 * Utility class of static methods.
 */
qx.Class.define("prearchiveui.Utils",
{
  type : "static",
  statics : {
    /**
     * Each session can have the following statuses
     */
    STATUS : ["READY", "RECEIVING", "ERROR", "ARCHIVING", "CONFLICT", "BUILDING","MOVING","_RECEIVING","_ARCHIVING","_BUILDING","_MOVING","_CONFLICT"],
    /**
     * The root URL of the XNAT application, eg. http://localhost:8080/xnat/
     */
    SERVER_ROOT : document.getElementById("serverRoot").title,
    UNASSIGNED : "Unassigned",

    /**
     * Merge two JSON objects. If there are similar keys the second map overwrites the first.
     * @param map1{Map} First map.
     * @param map2{Map} Second map which masks the first.
     */
    mergeMaps : function (map1, map2) {
      var ret = {};
      var key;
      for (key in map1) {
	ret[key] = map1[key];
      }
      for (key in map2) {
	ret[key] = map2[key];
      }
      return ret;
	},

    /**
     * Check if the session is locked.
     * @param session{Object} The given session.
     *
     */
    isLocked : function (session) {
      return prearchiveui.Utils.startsWith(session.status,'_');
    },

    /**
     * Check if the given string starts with the given character.
     * A null or empty string returns false.
     * @param s{String} Given string
     * @param c{Char} The character to match against.
     *
     */
    startsWith : function (s, c) {
      if (s != null && s.length >= 1) {
	return s.charAt(0) == c;
      }
      else {
	return false;
      }
    },

    /**
     * Remove the given prefix from the given string if it exists
     * @param s{String} Given string
     * @param prefix{String} Given prefix
     */
    removePrefix : function (s, prefix) {
      var index = s.indexOf(prefix);
      var ret = s;
      if (index != -1) {
	if (s.length == prefix.length) {
	  ret = "";
	}
	else {
	  ret = s.substring(prefix.length);
        }
      }
      return ret;
    },

    /** <pre>
     * This function accepts any number of arrays and returns an array of their common elements.
     * The idea is to create a temporary object whose keys are the elements of the arrays and whose value
     * holds the number of times that element was seen and returning those keys that whose values
     * are the number of arrays passed in. For example, given the following arrays :
     * [1,2,3,4], [2,3,4,5,6], [3,4,5,6,7,8]
     * the resulting object is :
     * {
     *   1:1,
     *   2:2,
     *   3:3,
     *   4:3
     * }
     * Since the '3' and '4' key are equal to number of arrays passed in these are the common elements and
     * the return array is [3,4].
     *
     * The pros of this approach are :
     * (1) The input arrays don't have to be sorted
     * (2) The size of the temporary object never exceeds the size of the shortest array
     * (3) Getting a key from a Javascript object is much faster than testing if an array contains an element.
     *
     * The cons are :
     * (1) Since input arrays don't have to be sorted every element of every input array must be processed
     *  thereby conserving memory at the expense of CPU usage.
     *
     * WARNING: This function only works on arrays of integers!
     * </pre>
     */

    intersection : function () {
      var arrays = [];
      for (var i = 0; i < arguments.length ; i++) {
        arrays.push(arguments[i]);
      }
      if (arrays.length < 2) {
        return arrays[0];
      }
      else {
        var intersect = {};
        arrays.sort(function (a1,a2) {
                      var ret = null;
                      if (a1.length < a2.length) { ret = -1;}
                      else if (a1.length == a2.length) { ret = 0; }
                      else {ret = 1;}
                      return ret;
                    });
        for (var array = 0; array < arrays.length ; array++) {
          for (var ind = 0; ind < arrays[array].length; ind++) {
            // for the first array just add as properties
            var prop = arrays[array][ind];
            if (array == 0) {
              intersect[prop] = 1;
            }
            else {
              if (intersect[prop] != null) {
                intersect[prop]++;
              }
            }
          }
        }
        var ret = [];
        for (prop in intersect) {
          if (intersect[prop] == arrays.length) {
            ret.push(parseInt(prop));
          }
        }
        return ret;
      }
    },

    /**
     * Create a label that accepts an html value.
     * @param v {String} An HTML string
     * @return {Label}
     */
    makeRichLabel : function (v){
      var label = new qx.ui.basic.Label();
      label.setRich(true);
      label.setValue(v);
      return label;
    },

    /**
     * Return the color that represents the status.
     * @param status {String} The status of this session
     * @return {String} a color string
     */
    statusColor : function (status){
      if (status == "BUILDING" || status == "QUEUED" || status == "QUEUED_BUILDING" || status == "Build pending") {
	return "#ccccff";
      }
      else if (status == "ARCHIVING" || status == "QUEUED_ARCHIVING" || status == "Archive pending") {
	return "#ffccff";
      }
      else if (status == "ERROR" || status == "Error") {
	return "#ff6666";
      }
      else  { // (status == "RECEIVING")
	return "#ccffff";
      }
      // Nothing is done for "READY"
    },

    showNotification : function () {
      prearchiveui.UserNotification.getInstance().show();
    },
    notify : function (str) {
      prearchiveui.UserNotification.getInstance().updateNotification(str);
    },
    hideNotification : function () {
      prearchiveui.UserNotification.getInstance().hide();
    },

    /**
     * Creates a progress bar, code mostly lifted from the
     * Qooxdoo demo browser (http://demo.qooxdoo.org/current/demobrowser/#progressive~ProgressiveLoader.html)
     */
    makeProgressive : function () {
      // We'll use the progressive table's progress footer.  For that, we
      // need to define column widths as if we were a table.
      var columnWidths = new qx.ui.progressive.renderer.table.Widths(1);
      columnWidths.setWidth(0, "100%");

      // Instantiate a Progressive
      var footer = new qx.ui.progressive.headfoot.Progress(columnWidths);
      var structure = new qx.ui.progressive.structure.Default(null, footer);
      var progressive = new qx.ui.progressive.Progressive(structure);

      // We definitely want to see each progress as we're loading.  Ensure
      // that the widget queue gets flushed
      progressive.setFlushWidgetQueueAfterBatch(true);


      // Instantiate a Function Caller
      var functionCaller = new qx.ui.progressive.renderer.FunctionCaller();

      // Give Progressive the renderer, and assign a name
      progressive.addRenderer("func", functionCaller);


      // Make the Progressive fairly small
      progressive.set(
        {
          height          : 50,
          width           : 272,
          zIndex          : 99999,
          backgroundColor : "gray",
          opacity         : 0.86,
          batchSize       : 1

        });
      return progressive;
    },

    /**
     * The dummy function. Consumes its input and does nothing.
     * @param x{Object} Can be anything.
     */
    dummy : function (x) {},

    /**
     * Identity function. Returns its input.
     * @param x{Object} Can be anything.
     */
    identity : function (x) {return x;},

    /**
     * Execute the given func some number of times.
     * @param func {Function} Function to execute
     * @param reps {Int} Number of times to execute func.
     */
    repeat : function (func, reps) {
      for (var i = 0; i < reps; i++) {
	func();
      }
    },

    /**
     *
     * Create and render a progress bar. It takes
     * @param elemFunc	     {Function}  the function to be performed at each progress step
     * @param elems	     {Int}       Number of steps to add the the progress bar at one time. Defaults to 1. If the # of elemFunc's is large
     *                                   adding them one at a time is slow.
     * @param batchSize	     {Int}       the number of 'elemFunc's the progress bar should do on each iteration. Defaults to 100.
     * @param container	     {Container} the container to which to add the created progress bar
     * @param context	     {Object}    the state that is kept persistent between each invocation of 'func'.
     *                                   For example, if 'context' is 1 , 'elems' is 3 and 'elemFunc' is:
     *                                   function (context)	{context++;}
     *                                   by the time the progress bar has run through its operations , context is 4.
     * @param preRenderFunc  {Function}  function to be performed before the progress bar has started rendering. Defaults to
     *                                   the dummy do-nothing function.
     * @param postRenderFunc {Function}  function to be performed after the progress bar completed rendering. Defaults to the
     *                                   dummy do-nothing function.
     *
     */
    addProgressive : function (elemFunc,
			       elems,
                               batchSize,
			       container,
			       context,
			       preRenderFunc,
			       postRenderFunc)

    {
      var render = true;
      if (null == elems) {
	elems = 1;
      }
      if (!batchSize) {
	batchSize = 100;
      }
      if (!container) {
	render = false;
      }
      if (!postRenderFunc) {
	postRenderFunc = prearchiveui.Utils.dummy;
      }
      if (!preRenderFunc) {
	preRenderFunc = prearchiveui.Utils.dummy;
      }

      var that = this;
      var progressBar = prearchiveui.Utils.makeProgressive();
      var progressive_data_model =  new qx.ui.progressive.model.Default();

      progressBar.setBatchSize(batchSize);

      var addFunc = function (func) {
	var ret = {
          renderer : "func",
          data     : func
        };
        return ret;
      };

      progressBar.addListener("renderStart",function (e) {
                                var state = e.getData().state;
				state.getUserData().context = context;
				preRenderFunc();
				container.getLayoutParent().add(progressBar);
                              }, this);

      progressBar.addListener("renderEnd",function(e) {
				container.getLayoutParent().remove(this);
				postRenderFunc();
			      });

      prearchiveui.Utils.repeat(function (){
				  progressive_data_model.addElement(addFunc(function (userData){
									      return elemFunc(userData.context);
									      }));
				}, elems);

      progressBar.setDataModel(progressive_data_model);
      return render ? progressBar.render():progressBar;
    },
      
    /**
     * A progressive widget for task where the number of sub-tasks is unknown.
     * Each invocation of a sub-task updates a flag inside the execution context that
     * determines if there is another sub-task.
     * 
     * @param elemFunc {Function} The sub-task
     * @param continueCase {Function} A function that takes the current context and returns
     *                                true if there are more sub-tasks and false otherwise
     * @param container	{Container} the container to which to add the created progress bar
     * @param visibility {String} The visibility setting of this container. Most often it should
     *                            be hidden because there is currently a bug where the progress bar shows 
     *                            100% even if there are more sub-tasks.
     * @param context {Map} The execution context of each sub-task.
     * @param preRenderFunc {Function} Invoked before the progress bar is rendered
     * @param postRenderFunc {Function} Invoked after the progress bar is completed
     * @param notify {Function} Invoked after the execution of each sub-task.
     * 
     */
    addUnboundedProgressive : function (elemFunc,
					continueCase,
					container,
					visibility,
					context,
					preRenderFunc,
					postRenderFunc,
				        notify){
      var render = true;
      var progressBar = prearchiveui.Utils.makeProgressive();
      var progressive_data_model =  new qx.ui.progressive.model.Default();

      if (!visibility) {
	visibility = "visible";
      }
      if (!container) {
	render = false;
      }
      if (!postRenderFunc) {
	postRenderFunc = prearchiveui.Utils.dummy;
      }
      if (!preRenderFunc) {
	preRenderFunc = prearchiveui.Utils.dummy;
      }
      if (!notify) {
	notify = prearchiveui.Utils.dummy;
      }
      
      var addFunc = function (func) {
	var ret = {
          renderer : "func",
          data     : func
        };
        return ret;
      };

      progressBar.addListener("renderStart",function (e) {
                                var state = e.getData().state;
				state.getUserData().context = context;
				preRenderFunc();
				container.add(progressBar);
				container.setVisibility(visibility);
                              }, this);

      progressBar.addListener("renderEnd",function(e) {
				container.remove(this);
				postRenderFunc();
			      });

      var step = function (userData) {
	if (continueCase(userData)) {
	  elemFunc(userData.context);
	  notify();
	  progressive_data_model.addElement(addFunc(step));
	}
      };

      progressive_data_model.addElement(addFunc(step));

      progressBar.setDataModel(progressive_data_model);
      return render ? progressBar.render():progressBar;
    }
  }
});











