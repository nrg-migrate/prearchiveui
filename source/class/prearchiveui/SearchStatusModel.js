/**
 * A class to manage the status filter
 */
qx.Class.define("prearchiveui.SearchStatusModel",
{
  extend : qx.core.Object,
  construct : function () {
    this.base(arguments);
    this.initView(new prearchiveui.SearchStatusView(["Archive pending","Archiving now","Build pending","Building now","Conflict","Delete pending","Deleting now","Error","Move pending","Moving now","Ready","Receiving"], this));
    this._addListeners();
  },
  events : {
    /**
     * See prearchiveui.SessionController for an explanation on this event
     */
    "changeSelection" : "qx.event.type.Data"
  },
  properties : {
    /**
     * A status string
     */
    status : {
      check : "String",
      init : "",
      apply : "_applyStatus"
    },
    /**
     * The view that displays this status filter
     */
    view : {
      check : "prearchiveui.SearchStatusView",
      deferredInit : true
    }
  },
  members : {
    /**
     * Set the current status
     * @param value{String} The current status
     */
    add : function (value) {
		this.setStatus(value);
    },
    _applyStatus : function (value,old) {
      if (value != old){
	this.fireDataEvent("changeSelection", this);
      }
    },
    _addListeners : function () {
      this.getView().getSelectBox().addListener("changeSelection", function (e) {
						 var item = e.getData()[0];
						 this.add(item.getLabel());
					       },this);
    }
  }
});
