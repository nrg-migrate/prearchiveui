/**
 * A preview pane that displays actions for the rows selected. Should only be instantiated once by the controller.
 */
qx.Class.define("prearchiveui.SessionActions",
{
  extend : qx.core.Object,
  construct : function () {
    this.base(arguments);
    this.__unique_id = -1;
  },
  events : {
    /**
     * See prearchiveui.SessionController for explanation on this event.
     */
    "changeAction" : "qx.event.type.Data"
  },
  members : {
    __sessionActionsStack : null,
    __sessionActionsContainer : null,
    __blocker : null,
    __sessionActions : null,
    __buttonPane : null,
	__actionsLabel : null,
    __unique_id : null,
    __buttons : [
      {name : "Archive", action : "Archive",enabled : prearchiveui.Sessions.ARCHIVABLE, button : null},
      {name : "Review and Archive", action : "Review", enabled : prearchiveui.Sessions.ARCHIVABLE, button : null},
	  {name : "Change Projects", action : "Move", enabled : prearchiveui.Sessions.MOVABLE, button : null},
      {name : "Delete", action : "Delete", enabled : prearchiveui.Sessions.DELETABLE, button : null},
      {name : "Rebuild", action : "Reset", enabled : prearchiveui.Sessions.RESETABLE, button : null}
    ],
    __richLabel : function (v) {
      var label = new qx.ui.basic.Label();
      label.setRich(true);
      label.setValue(v);
      return label;
    },
    __addLabel : function (v, r, c) {
      this.__sessionActionsContainer.add(this.__richLabel(v), {row : r, column : c});
    },
    __addInfo : function (v,r,c) {
      if (v != null) {
	if (this.__sessionActionsContainer.getLayout().getCellWidget(r,c) != null) {
          this.__sessionActionsContainer.getLayout().getCellWidget(r,c).setValue(v.toString());
	}
	else {
      	  this.__addLabel(v.toString(),r,c);
	}
      }
    },
    __enableDisableButtons : function (session) {
      var that = this;
      this.__buttons.map(function (spec) {
			   var enabled = that.__unique_id != null && that.__unique_id != -1 && spec.enabled(session);
			   spec.button.setEnabled(enabled);
			 });
    },
    __initButtonPane : function () {
	
      var paneLayout = new qx.ui.layout.HBox(2);

      this.__buttonPane = new qx.ui.container.Composite(paneLayout).set({
								       paddingTop: 0
								     });

     this.__buttons.map(function (spec) {
					var listener = function (e) {
					  this.fireDataEvent("changeAction", {
							       type : "bulk",
							       action : spec.action,
							       id : this.__unique_id
							     },this);
					};
					var archiveListener = function (e) {
					  this.fireDataEvent("changeAction", {
							       type : "archive",
							       action : spec.action,
							       id : this.__unique_id
							     },this);
					};
					var archiveReviewListener = function (e) {
					  this.fireDataEvent("changeAction", {
							       type : "review",
							       action : spec.action,
							       id : this.__unique_id
							     },this);
					};
					if(spec.name=="Archive"){
					  spec.button = this.__createButton(spec.name,"execute",archiveListener,this);
					}
					else if(spec.name=="Review and Archive"){
					  spec.button = this.__createButton(spec.name,"execute",archiveReviewListener,this);
					}
					else{
					  spec.button = this.__createButton(spec.name,"execute",listener,this);
					}
					spec.button.setEnabled(true);
					
						  
					this.__buttonPane.add(spec.button);
				      },this);


	  this.__addInfo("Actions are performed on checked sessions.",0,0);	  
      this.__sessionActionsContainer.add(this.__buttonPane,{row:1, column:0});
    },
    __createButton : function (name, event, func, cxt) {
      var button = new qx.ui.form.Button(name);
      if (event != null) {
      	if (func != null) {
      	  button.addListener(event,func,cxt);
      	}
      }
      return button;
    },

    __initSessionActions : function () {
      var that = this;
      var title = function (v) {
	var label = that.__richLabel(v);
	label.setTextAlign("center");
	return label;
      };
      this.__initButtonPane();
    },

    __applySessionShowing : function (newData, oldData) {
    },
    __showNewSessActions : function (data) {
    },
    /**
     * Create the actions box.
     */
    create : function () {
      this.__sessionActionsStack = new qx.ui.container.Stack(new qx.ui.layout.VBox());
      this.__sessionActionsStack.setMinWidth(315);
      this.__blocker = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
      this.__blocker.setBackgroundColor("yellow");

      this.__sessionActionsContainer = new qx.ui.groupbox.GroupBox("Actions");
      this.__sessionActionsContainer.setLayout(new qx.ui.layout.Grid(8,8));

      this.__initSessionActions();
      this.__sessionActionsStack.add(this.__sessionActionsContainer);
      this.__sessionActionsStack.add(this.__blocker);
	  
      return this.__sessionActionsStack;
    },
    /**
     * Update the actions with the given session data
     * @param sessions{Object} Given sessions.
     */
    onUpdate : function (sessions, checkedSessions) {
	  if(sessions.length>0){
        this.__unique_id = sessions[0].unique_id;
        this.debug(sessions[0].url);
	  }
	  else{
	    this.clear();
	  }
    },
    /**
     * Clear the actions box.
     */
    clear : function () {
      this.__unique_id = -1;
      this.__buttons.map(function(spec) {
			   spec.button.setEnabled(true);
			 });
    },
    /**
     * Clear the actions box if given index matches the session being displayed.
     * @param index{Int} Unique session id
     */
    remove : function (index) {
      if (this.__unique_id == index) {
	this.clear();
	this.__unique_id = null;
      }
    }
  }
});
