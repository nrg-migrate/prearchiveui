/**
 * Creates the window that confirms moving sessions to a different project
 */
qx.Class.define("prearchiveui.MoveConfirmationDialogView",
{
  extend : prearchiveui.ConfirmationDialogView,
  /**
   * @param title{String} The title of the dialog window.
   * @param message{String} The message in the dialog window.
   * @param okAction{Function} Run if the user confirms
   * @param cancelAction{Function} Run if the user cancels.
   * @param projNames{Array} List of projects to which the user can move sessions.
   */
  construct : function (title,message,okAction,cancelAction,projNames) {
    this.__projNames = projNames;
    this.base(arguments,title,message,okAction,cancelAction);

  },
  properties : {
    /**
     * Select box which displays the projects to which the user can move sessions.
     */
    selectBox : {
      check : "qx.ui.form.SelectBox",
      deferredInit : true
    },
    /**
     * The form widget that contains the project selection box and message.
     */
    form : {
      check : "qx.ui.form.Form",
      deferredInit : true
    }
  },
  members : {
    _initMessage : function () {
      this.base(arguments);

      this.__projNames = new qx.type.Array().concat(this.__projNames);

      this.initSelectBox(new qx.ui.form.SelectBox());

      this.__projNames.sort(function(name1, name2)
    {
      return name1.toLowerCase().localeCompare(name2.toLowerCase());
    }).forEach(function (projName) {
				    this.getSelectBox().add(new qx.ui.form.ListItem(projName));
			       },this);
      this.initForm(new qx.ui.form.Form());
      this.getForm().add(this.getSelectBox(),"Select a project");
    },

    _initButtons : function (){
      var ok = new qx.ui.form.Button("Ok", "icon/16/actions/dialog-ok.png");
      var cancel = new qx.ui.form.Button("Cancel", "icon/16/actions/dialog-cancel.png");
      this._initListeners(ok,cancel);
      this.getForm().addButton(ok);
      this.getForm().addButton(cancel);
    },
    _init : function () {
      this.base(arguments);
      this.getWindow().add(new qx.ui.form.renderer.Single(this.getForm()));
    },

    _initListeners : function (ok,cancel) {
      ok.addListener("execute", function (e) {
		       if (this.getForm().validate()) {
			 this.getWindow().close();
			 this.getOkAction()(this.getSelectBox().getSelection()[0].getLabel());
		       }
		     },this);
      cancel.addListener("execute", function(e) {
			   this.getWindow().close();
			   this.getCancelAction()();
			 },this);
    }

  }
});
