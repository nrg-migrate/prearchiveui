/**
 * Cell renderer that adds a background color to the status cell of the sessions table.
 */
qx.Class.define("prearchiveui.StatusRenderer",
{
  extend : qx.ui.table.cellrenderer.Default,
  members : {
    _getCellStyle: function (cellInfo) {
      if (cellInfo.value == "READY" || cellInfo.value == "Ready") {
      }
      else {
	return "background-color:" + prearchiveui.Utils.statusColor(cellInfo.value);
      }
    }
  }
});
