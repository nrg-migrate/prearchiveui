/**
 * Creates the window that allows the user to choose the number of days for which to view DICOM headers
 */
qx.Class.define("prearchiveui.HeaderConfirmationDialog",
{
  extend : prearchiveui.ConfirmationDialogView,
  /**
   * @param title{String} The title of the dialog window.
   * @param message{String} The message in the dialog window.
   * @param okAction{Function} Run if the user confirms
   * @param cancelAction{Function} Run if the user cancels.
   * @param projNames{Array} List of projects to which the user can move sessions.
   */
  construct : function (title,message,okAction,cancelAction,maxDays) {
    this.initMaxDays(maxDays);
    this.base(arguments,title,message,okAction,cancelAction);
  },
  properties : {
    maxDays : {
      check : "Integer",
      deferredInit : true
    },
    /**
     * Select box which displays the projects to which the user can move sessions.
     */
    selectBox : {
      check : "qx.ui.form.SelectBox",
      deferredInit : true
    },
    /**
     * The form widget that contains the project selection box and message.
     */
    form : {
      check : "qx.ui.form.Form",
      deferredInit : true
    }
  },
  members : {
    _initMessage : function () {
      this.base(arguments);
      var daysSelection = [];
      for (var i = 1; i < this.getMaxDays() + 1; i++) {
	daysSelection.push(i.toString());
      }

      this.initSelectBox(new qx.ui.form.SelectBox());
      daysSelection.forEach(function (day) {
			      this.getSelectBox().add(new qx.ui.form.ListItem(day));
			    },this);
      this.initForm(new qx.ui.form.Form());
      this.getForm().add(this.getSelectBox(),"Select number of days: ");
    },

    _initButtons : function (){
      var ok = new qx.ui.form.Button("Ok", "icon/16/actions/dialog-ok.png");
      var cancel = new qx.ui.form.Button("Cancel", "icon/16/actions/dialog-cancel.png");
      this._initListeners(ok,cancel);
      this.getForm().addButton(ok);
      this.getForm().addButton(cancel);
    },
    _init : function () {
      this.base(arguments);
      this.getWindow().add(new qx.ui.form.renderer.Single(this.getForm()));
    },

    _initListeners : function (ok,cancel) {
      ok.addListener("execute", function (e) {
		       if (this.getForm().validate()) {
			 this.getWindow().close();
			 this.getOkAction()(parseInt(this.getSelectBox().getSelection()[0].getLabel()));
		       }
		     },this);
      cancel.addListener("execute", function(e) {
			   this.getWindow().close();
			   this.getCancelAction()();
			 },this);
    }

  }
});
