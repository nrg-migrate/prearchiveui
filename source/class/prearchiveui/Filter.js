qx.Class.define("prearchiveui.Filter",
{
  extend : qx.core.Object,
  construct : function (func, type) {
    this.base(arguments);
    this.initFunc(func);
    this.initType(type);
  },
  statics : {
    id : function (v) {return true;}
  },
  properties : {
    func : {
      check : "Function",
      deferredInit : true
    },
    type : {
      check : function(value) {
	return (value != null) && ((value == "Intersection") || (value == "Union"));
      },
      deferredInit : true
    }
  },
  members : {
    __negate : function () {
      return (function (v) {
		return (!this.getFunc()(v));
	      });
    },
    __filter : function (func, vs, cxt) {
      var filter = qx.lang.Core.arrayFilter;
      vs.filter(this.getFunc(),vs,cxt);
    },
    apply : function (vs,cxt) {
      this.__filter(this.getFunc(), vs,cxt);
    },
    notFilter : function (vs,cxt) {
      return new prearchiveui.Filter(this.__negate(), this.getType());
    }
  }
});
