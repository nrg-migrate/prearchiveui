/**
 * A class that sets the cap on the number of entries that can be displayed
 * by the TokenField's drop-down menu
 */
qx.Class.define("prearchiveui.SearchRestrictedTextfieldModel",
{
  extend : prearchiveui.SearchTextfieldModel,
  /**
   * 
   */
  construct : function (dataFunc, hintText, cap) {
    this.base(arguments, dataFunc, hintText);
    if (cap != null) {
      this.getView().setTextfield(new prearchiveui.RestrictedToken(cap));
    }
    else {
      this.getView().setTextfield(new prearchiveui.RestrictedToken());
    }
    this._addListeners();
  }
});
