/**
 * A class that manages the search text fields.
 */
qx.Class.define("prearchiveui.SearchTextfieldModel",
{
  extend : qx.core.Object,
  /**
   * @param dataFunc{Function} A function that processes the text the user types into the textfield.
   * @param hintText{String} Greyed-out text to show in an empty textfield
   * @param cap{Int} The maximum number of items to show in the popup
   */
  construct : function (dataFunc, hintText,cap) {
    this.base(arguments);
    this.initValues(new qx.type.Array());
    this.initView(new prearchiveui.SearchTextfieldView(hintText, this, cap));
    this.initDataFunc(dataFunc);
    this._addListeners();
  },
  events : {
    /**
     * See prearchiveui.SessionController for explanation about this event.
     */
    "changeSelection" : "qx.event.type.Data"

  },
  properties : {
    /**
     * A list of selectected items.
     */
    values : {
      check : "qx.type.Array",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    dataFunc : {
      check : "Function",
      deferredInit : true
    },
    /**
     * The view that displays this textfield.
     */
    view : {
      deferredInit : true
    }
  },
  members : {
    /**
     * Add a users search text to the list of values.
     */
    add : function (item) {
      this.getValues().push(item);
      this.fireDataEvent("changeSelection", this);
    },
    /**
     * Remove a users search text from the list of values.
     */
    remove : function (item) {
      qx.lang.Array.exclude(this.getValues(), [item]);
      this.fireDataEvent("changeSelection", this);
    },

    addManually : function (item) {
      this.getView().getTextfield().selectItem(item);
    },

    _addListeners : function () {
      this.getView().getTextfield().addListener("addItem",function(e) {
		      var item = e.getData();
		      this.add(item.getLabel());
		    },this);
      this.getView().getTextfield().addListener("removeItem",function(e) {
		      var item = e.getData();
		      this.remove(item.getLabel());
		    },this);
      this.getView().getTextfield().addListener("loadData", function(e) {
		      var str = e.getData();
		      var data = this.getDataFunc()(str);
		      this.getView().getTextfield().populateList(str, data);
		    },this);
    },
    /**
     * Return the widget that creates this search textfield.
     */
    widget : function () {
      return this.getView().getWidget();
    }
  }
});
