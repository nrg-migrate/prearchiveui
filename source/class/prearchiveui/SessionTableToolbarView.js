/**
 * A class to draw and manage the prearchive ui toolbar.
 */
qx.Class.define("prearchiveui.SessionTableToolbarView",
{
  extend : qx.core.Object,
  construct : function () {
    this.base(arguments);
  },
  events : {
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeView"	        : "qx.event.type.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeAction"		: "qx.event.type.Data",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeUnselectAll"		: "qx.event.type.Event",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeRefreshAll"		: "qx.event.type.Event",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeSelectAll"		: "qx.event.type.Event",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeSelect"		: "qx.event.type.Event",
    /**
     * See prearchiveui.SessionController for event description
     */
    "changeUnselect"	        : "qx.event.type.Event",
    /**
     * 
     */
    "dicomheaderview"           : "qx.event.type.Data"
  },
  members : {
    __viewsSelector : null,
    __views : {
      All : null,
      Checked : null,
      Filtered : null
    },

    /**
     * Change the selector to the given view.
     * @param str{String} The given view
     */
    setSelection : function (str) {
      var items = this.__viewsSelector.getChildren();
      for (var i = 0; i < items.length; i++) {
	if (items[i].getLabel() == str){
	  this.__viewsSelector.setSelection([items[i]]);
	}
      }
    },

    /**
     * Create the toolbar
     */
    create : function () {
      var viewsButton = new qx.ui.form.MenuButton("View");
	  viewsButton.setIcon("qx/decoration/Modern/arrows/down.png");
	  viewsButton.setIconPosition("right");
      var viewsMenu = new qx.ui.menu.Menu();
      
      var all = new qx.ui.menu.RadioButton("All");
      var selected = new qx.ui.menu.RadioButton("Checked");
      var filtered = new qx.ui.menu.RadioButton("Filtered");

      this.__views["All"] = all;
      this.__views["Checked"] = selected;
      this.__views["Filtered"] = filtered;

      var viewsSelector = new qx.ui.form.RadioGroup();
      viewsSelector.add(all,selected,filtered);
      viewsSelector.addListener("changeSelection", function (e) {
				  var selection = e.getData()[0].getLabel();
				  this.fireDataEvent("changeView", {view : selection});
				}, this);

      viewsMenu.add(all);
      viewsMenu.add(selected);
      viewsMenu.add(filtered);
      viewsButton.setMenu(viewsMenu);
      this.__viewsSelector = viewsSelector;

      var actionsButton = new qx.ui.form.MenuButton("Actions");
	  actionsButton.setIcon("qx/decoration/Modern/arrows/down.png");
	  actionsButton.setIconPosition("right");
      var actionsMenu = new qx.ui.menu.Menu();

      var archive = new qx.ui.menu.Button("Archive");
      var revarchive = new qx.ui.menu.Button("Review and Archive");
      var move = new qx.ui.menu.Button("Change Projects");
      var del = new qx.ui.menu.Button("Delete");

      actionsMenu.add(archive);
      actionsMenu.add(revarchive);
	  actionsMenu.add(move);
      actionsMenu.add(del);

      archive.addListener("execute", function (e) {
			   this.fireDataEvent("changeAction", {type : "bulk", action : "Archive"});
			 },this);
	  revarchive.addListener("execute", function (e) {
			   this.fireDataEvent("changeAction", {type : "single", action : "Review"});
			 },this);
      del.addListener("execute", function (e) {
			   this.fireDataEvent("changeAction", {type : "bulk", action : "Delete"});
			 },this);
      move.addListener("execute", function (e) {
			   this.fireDataEvent("changeAction", {type : "bulk", action : "Move"});
			 },this);


      actionsButton.setMenu(actionsMenu);
      var that = this;

      var createButton = function (archive, days, label) {
	var button = new qx.ui.menu.Button(label);
	button.addListener("execute", function (e){
			     that.fireDataEvent("dicomheaderview", {type : "header",
								    archive : archive,
								    numDays : days});
			   });
	return button;
      };
      var createRecentMenu = function (archive) {
      	var recentMenu = new qx.ui.menu.Menu();
	recentMenu.add(createButton(archive, 5, "5 days"));
	recentMenu.add(createButton(archive, 10, "10 days"));
	recentMenu.add(createButton(archive, 15, "15 days"));
	recentMenu.add(createButton(archive, -1, "Other"));
	return recentMenu;
      };

      var archiveHeaders = new qx.ui.menu.Button("Archive");
      var prearchiveHeaders = new qx.ui.menu.Button("Prearchive");

      archiveHeaders.setMenu(createRecentMenu("archive"));
      prearchiveHeaders.setMenu(createRecentMenu("prearchive"));

      var headersMenu = new qx.ui.menu.Menu();
      headersMenu.add(archiveHeaders);
      headersMenu.add(prearchiveHeaders);
      
	   /** 
      var headersButton = new qx.ui.form.MenuButton("DICOM Headers");
	  headersButton.setIcon("qx/decoration/Modern/arrows/down.png");
	  headersButton.setIconPosition("right");
      headersButton.setMenu(createRecentMenu("prearchive"));
	   */
	   

	  var refreshAll = new qx.ui.form.Button("Refresh");
      refreshAll.addListener("execute", function (e) {
				this.fireEvent("changeRefreshAll");
			  },this);
			  
	  var selectAll = new qx.ui.form.Button("Check All");
      selectAll.addListener("execute", function (e) {
				this.fireEvent("changeSelectAll");
			  },this);

	  var unselectAll = new qx.ui.form.Button("Uncheck All");
      unselectAll.addListener("execute", function (e) {
				this.fireEvent("changeUnselectAll");
			  },this);
			 
      var unselect = new qx.ui.form.Button("Uncheck");
      unselect.addListener("execute", function (e) {
			   this.fireEvent("changeUnselect");
			 },this);

      var select = new qx.ui.form.Button("Check");
      select.addListener("execute", function (e) {
			   this.fireEvent("changeSelect");
			 },this);

      var bar = new qx.ui.toolbar.ToolBar();
      var buttonsBox = new qx.ui.layout.HBox(6,"right");
	   this.__dropdownbuttons = new qx.ui.container.Composite(buttonsBox);
	   this.__dropdownbuttons.add(viewsButton);
	   /** this.__dropdownbuttons.add(actionsButton); */
	   /** this.__dropdownbuttons.add(headersButton); */
      bar.add(this.__dropdownbuttons);

      bar.addSpacer();
      bar.add(refreshAll);
	  bar.add(select);
      bar.add(unselect);
	  bar.add(selectAll);
	  bar.add(unselectAll);
      return bar;
    }
  }
});
