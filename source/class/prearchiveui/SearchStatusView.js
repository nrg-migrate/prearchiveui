/**
 * A class that creates a select box of statuses
 */
qx.Class.define("prearchiveui.SearchStatusView",
{
  extend : qx.core.Object,
  /**
   * @param statuses{Array} A list of statuses to display in this combo-box
   */
  construct : function (statuses) {
    this.base(arguments);
    var selectBox = new qx.ui.form.SelectBox();

    // empty status
    selectBox.add(new qx.ui.form.ListItem(""));
    statuses.forEach(function (s) {
		     selectBox.add(new qx.ui.form.ListItem(s));
		   });
    this.initSelectBox(selectBox);
    // initialize to the empty status
  },
  properties : {
    /**
     * Select box that displays the statuses
     */
    selectBox : {
      check : "qx.ui.form.SelectBox",
      deferredInit : true
    }
  }
});
