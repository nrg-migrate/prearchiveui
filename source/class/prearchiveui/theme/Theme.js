/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("prearchiveui.theme.Theme",
{
    meta :
    {
	color      : tokenfield.theme.modern.Color,
	decoration : tokenfield.theme.modern.Decoration,
	font       : tokenfield.theme.modern.Font,
	icon       : qx.theme.icon.Tango,
	appearance : tokenfield.theme.modern.Appearance
    }
});
