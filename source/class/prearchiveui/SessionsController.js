/**
 * <pre>
 *
 * The controller of the toolbar, sessions database, the details view and the search box.
 *
 * This class should only be instantiated once.
 *
 * UI Layout
 * =========
 *
 *   <-------------------__rootContainer--------------------->
 *   <------------------__container (HBox)------------------->
 *   <-----__tableContainer(VBox)----><--__detailsPane(VBox)->
 *   +-------------------------------------------------------+
 *   |         __toolbar             |                       |
 *   |      SessionToolbarView       |                       |
 *   +-------------------------------+                       |
 *   |                               |       __details       |
 *   |                               |    SessionDetails     |
 *   |                               +-----------------------+
 *   |                               |       _actions        |
 *   |        __table                |    SessionActions     |
 *   |    SessionTableView           +-----------------------+
 *   |                               |                       |
 *   |                               |       __searchBox     |
 *   |                               |   SearchBoxView       |
 *   |                               |                       |
 *   |                               |                       |
 *   +-------------------------------+-----------------------+
 *
 * Events
 * ======
 * All events fired by each of the UI classes is caught and handled here.
 *
 * | Component               | Event                 | Trigger                           | Data                                   |
 * +-------------------------+-----------------------+-----------------------------------+----------------------------------------+
 * | SessionTableToolbarView | changeView            | User changes to a different view  | {view : view-name                      |
 * |                         | changeAction          | A bulk session action is invoked. | {type : "bulk", action: action-name}   |
 * |                         | changeSelectAll       | "Select All" button pressed       |                                        |
 * |                         | changeUnselectAll     | "Unselect All" button pressed     |                                        |
 * |                         | changeSelect          | Bulk "Select" button pressed      |                                        |
 * |                         | changeUnselect        | Bult "Unselect" button pressed    |                                        |
 * |                         | changeRefreshAll      | "Refresh" button pressed     |                                        |
 * | SessionDetails          | changeAction          | A session action is invoked       | {type : "single", action: action-name} |
 * |                         |                       |
 * | SessionTableView        | changeSessionSelected | User selected session/s           | [unique_id1, unique_id2 ...]           |
 * |                         | created               | Table has completed rendering     |                                        |
 * |                         | changeSelect          | User checked the row check box    |                                        |
 * |                         | changeUnselect        | User unchecked the row check box  |                                        |
 * | SearchTextfieldModel    | changeSelection       | User added project/subject token  | this                                   |
 * | DateRangeModel          | changeDateRange       | User selected a valid date range  | this                                   |
 * | SearchStatus            | changeSelection       | User selected a new status        | this                                   |
 * | Sessions                | loaded                | Received and parsed sessions      |                                        |
 *
 * </pre>
 */
qx.Class.define("prearchiveui.SessionsController",
{
  extend : qx.core.Object,
  /**
   * @param container{qx.ui.root.Inline} The root container for the prearchive
   * @param serverRoot{String} The path to the xnat instance: eg. http://localhost:8080/xnat/
   */
  construct : function (container, serverRoot) {
    this.base(arguments);
    this.__sessions = new prearchiveui.Sessions("/REST/prearchive", "csv");
    this.__serverRoot = serverRoot;
    this.__rootContainer = container;

    this.__rootContainer.setZIndex(0);
    this.__initProject = this.__lookForInitialProject();
    this.__detailsPane = new qx.ui.container.Composite(new qx.ui.layout.VBox());
    this.__container = new qx.ui.container.Composite(new qx.ui.layout.HBox());

    this.__rootContainer.add(this.__container);
    var that = this;
    this.__sessions.addListener("loaded", function (e){
				  this.__initViews();
                                },this);
    this.__views = [];
    this.__tableContainer = new qx.ui.groupbox.GroupBox("Sessions");
    this.__tableContainer.setLayout(new qx.ui.layout.VBox());
  },

  members : {
    // The session store
    __sessions : null,

    // xnat application root
  __serverRoot : null,

    // The project value in case the user gets to this ui from the project page.
    __initProject : null,

    // UI Elements. See the diagram in the header comment to see what they mean.
    __toolbar : null,
    __table : null,
    __details : null,
	__actions : null,
	__container : null,
    __detailsPane : null,
	__tableContainer : null,
    __rootContainer : null,
    __searchBox : null,
    __searchBoxContainer : null,
    __views : null,


    /**
     * An object which holds filtered lists of search matches. The value of each filter can be one of three things:
     * 1. []                - nothing matches the user search
     * 2. [sess1 ... sessN] - a set of sessions that matches the user input
     * 3. -1                - this filter is disabled.
     * All filters are initially disabled.
     *
     */
    __filters : {
      project : -1 ,
      upload_date : -1,
      session_date : -1,
      subjectOrSession : -1,
      status : -1
    },
    /**
     * A store of current session ids that match the search criteria. This variable is kept in sync with changing search constraints
     * A -1 is the base value indicating that there are no matching sessions given the search criteria.
     */
    _intersection : -1,

    /**
     * Updates _intersection with the sessions ids that match the search criteria. It also changes to a filtered view if necessary.
     * @param show {Boolean} If true, switch to the Filtered view after setting the intersection.
     */
    __setIntersection : function (show) {
      this._intersection = this.__intersectionFilters();
      if (this._intersection == -1) {
	  this.__table.setFiltered([]);
	  this.__toolbar.setSelection("All");
      }
    else {
	this.__table.setFiltered(this._intersection);
	if (show) {
	  this.__toolbar.setSelection("Filtered");
	}
      }
    },

    __lookForInitialProject : function () {
      return document.getElementById("project").title;
    },

    /**
     * Block the prearchiveui screen. Necessary for operations that use a progress bar.
     */
    block : function () {
      var blocker = new qx.ui.core.Blocker(this.__rootContainer);
      blocker.setOpacity(0.3);
      blocker.setColor("blue");
      this.__blocker = blocker;
      this.__blocker.block();
    },

    /**
     * Unblock the prearchiveui screen.
     */
    unblock : function ()  {
      this.__blocker.unblock();
    },

    /**
     * Listener for all events. For an explanation of the events and the data they carry see the class header comment.
     */
    __addListeners : function (){
      var that = this;
      this.__table.addListener("changeSessionSelection", function (e) {
                                 var data = e.getData();
                                 this.details(data);
                               },this);
      this.__toolbar.addListener("changeView", function (e) {
				   var data = e.getData();
				   if (data.view == "Checked"){
				     this.__table.showSelected();
				   }
				   else if (data.view == "Filtered") {
				     this.__table.showFiltered();
				   }
				   else { // (data.view == "All")
				     this.__table.showAll();
				   }
                                 },this);
      this.__toolbar.addListener("changeRefreshAll", function (e) {
                                   this.__table.refreshAll(this);
                                 },this);
      this.__toolbar.addListener("changeSelectAll", function (e) {
                                   this.__table.selectAll(this);
                                 },this);
	  this.__toolbar.addListener("changeUnselectAll", function (e) {
                                   this.__table.unselectAll(this);
                                 },this);
      this.__toolbar.addListener("changeSelect", function (e) {
                                   this.__table.selectSelection(this);
                                 },this);
      this.__toolbar.addListener("changeUnselect", function (e) {
                                   this.__table.unselectSelection(this);
                                 },this);
      this.__toolbar.addListener("changeAction", function (e) {
				   this.__processAction(e.getData());
				 },this);
      this.__toolbar.addListener("dicomheaderview", function (e) {
				   var data = e.getData();
				   this.__processAction(data);
				 },this);
	  this.__details.addListener("statusAction", function (e) {
				   this.__displayStatus(e.getData());
				 },this);
      this.__details.addListener("changeAction", function (e) {
				   this.__processAction(e.getData());
				 },this);
	  this.__actions.addListener("changeAction", function (e) {
				   this.__processAction(e.getData());
				 },this);
      this.__table.addListener("created", function (e) {
                                 this.__detailsPane.add(this.__details.create());
								 this.__detailsPane.add(this.__actions.create());
				 this.__detailsPane.add(this.__searchBox.create());
                                 this.__container.add(this.__detailsPane);
				 if (this.__initProject) {
				   this.__searchProjects.addManually(this.__initProject);
				 }
                               },this);

      this.__table.addListener("changeSelect", function (e) {
                                   this.__table.selectSelection(this);
                                 },this);
      this.__table.addListener("changeUnselect", function (e) {
                                   this.__table.unselectSelection(this);
                                 },this);

      this.__searchUploadDate.addListener("changeDateRange", function (e) {
					    var data = e.getData();
					    if (data.isActive()){
					      this.__filters.upload_date = this.__sessions.getSessionsUploadedBetween(data.getFirst(),data.getSecond());
					    }
					    else {
					      this.__filters.upload_date = -1;
					    }
					    this.__setIntersection(true);
					  }, this);
      this.__searchScanDate.addListener("changeDateRange", function (e){
					  var data = e.getData();
					    if (data.isActive()){
					      this.__filters.session_date = this.__sessions.getSessionsScannedBetween(data.getFirst(),data.getSecond());
					    }
					    else {
					      this.__filters.session_date = -1;
					    }
					      this.__setIntersection(true);
					  }, this);
      this.__searchProjects.addListener("changeSelection", function (e) {
					  var data = e.getData();
					  this.__filters.project = data.getValues().length ==0 ? -1 : this.__sessions.getSessionsInProjects(data.getValues());
					  this.__setIntersection(true);
					},this);
      this.__searchSubjectSessions.addListener("changeSelection", function (e) {
						 var data = e.getData();
						 this.__filters.subjectOrSession = data.getValues().length == 0 ?  -1 : this.__sessions.getSessionsBySubjectOrSession(data.getValues());
						 this.__setIntersection(true);
					       },this);
      this.__searchStatus.addListener("changeSelection", function (e) {
					var data = e.getData();
					this.__filters.status = data.getStatus() != "" ? this.__sessions.getSessionsWithStatus(data.getStatus()) : -1;
					this.__setIntersection(true);
				      },this);

    },

    /**
     * This methods returns the unique session ids common to all the filters.
     */
    __intersectionFilters : function () {
      var filters = [];
      // ignore disabled filters
      for (var prop in this.__filters) {
        if (this.__filters[prop] != -1){
          filters.push(this.__filters[prop]);
        }
      }
      if (filters.length != 0) {
        return prearchiveui.Utils.intersection.apply(this, filters);
      }
      else {
        return -1;
      }
    },

    /**
     * Manually recalculate filters
     * @param show {Boolean} whether to switch to the filtered view after recalculating it.
     */
    __recalculateFilters : function (show) {
      if (this.__searchScanDate.isActive()){
	this.__filters.session_date = this.__sessions.getSessionsScannedBetween(this.__searchScanDate.getFirst(),this.__searchScanDate.getSecond());
      }
      else {
	this.__filters.session_date = -1;
      }
      if (this.__searchUploadDate.isActive()){
	this.__filters.session_date = this.__sessions.getSessionsScannedBetween(this.__searchUploadDate.getFirst(),this.__searchUploadDate.getSecond());
      }
      else {
	this.__filters.session_date = -1;
      }

      this.__filters.project = this.__searchProjects.getValues().length ==0 ? -1 : this.__sessions.getSessionsInProjects(this.__searchProjects.getValues());
      this.__filters.subjectOrSession =this.__searchSubjectSessions.getValues().length == 0 ?  -1 : this.__sessions.getSessionsBySubjectOrSession(this.__searchSubjectSessions.getValues());
      this.__filters.status =this.__searchStatus.getStatus() != "" ? this.__sessions.getSessionsWithStatus(this.__searchStatus.getStatus()) : -1;
    },

    /**
     * Create the prearchive UI.
     */
    __initViews : function () {
      this.__details = new prearchiveui.SessionDetails();
	  this.__actions = new prearchiveui.SessionActions();
	this.__toolbar = new prearchiveui.SessionTableToolbarView();
      var that = this;
      // The function that will takes a string and returns names of projects that have that
      // in their name. Used to populate a list of possible
      // projects in the search-as-you-type project field.
      var getProjects = function (str) {
	var _projects = that.__sessions.getProjects();
	var data = [];
	if (str.length == 1) {
          for (var i = 0; i < _projects.length; i++) {
            if (_projects[i] == str) {
              data.push({label : _projects[i]});
	    }
	  }
	}
	else {
          for (var i = 0; i < _projects.length; i++) {
            if (qx.lang.String.contains(_projects[i], str)) {
              data.push({label : _projects[i]});
	    }
	  }
	}
	return data;
      };

      // The function that will takes a string and returns names of sessions *or* subjects that have that
      // in that name. Used to populate a list of possible sessions or subjects
      // in the search-as-you-type session/subject field.
      var getSessionSubjects = function (str) {
	var _sessionSubjects = that.__sessions.getSessions(that._intersection);
	var tmp = {};
	if (str.length == 1) {
          for (var i = 0; i < _sessionSubjects.length; i++) {
            var found = false;
            if (_sessionSubjects[i].name == str) {
              tmp[_sessionSubjects[i].name] = true;
              found = true;
            }
            if (_sessionSubjects[i].subject == str && !found) {
              tmp[_sessionSubjects[i].subject] = true;
	    }
	  }
	}
	else {
          for (var i = 0; i < _sessionSubjects.length; i++) {
            var found = false;
            if (qx.lang.String.contains(_sessionSubjects[i].name, str)) {
              tmp[_sessionSubjects[i].name] = true;
              found = true;
            }
            if (qx.lang.String.contains(_sessionSubjects[i].subject, str) && !found) {
              tmp[_sessionSubjects[i].subject] = true;
	    }
	  }
	}
	var data = [];
	for (name in tmp) {
	  data.push({label: name});
	}
	return data;
      };


      // The search-as-you-type project textfield
      this.__searchProjects = new prearchiveui.SearchTextfieldModel(getProjects,
								    "Type project name here",
								    1000);

      // The status combo box
      this.__searchStatus = new prearchiveui.SearchStatusModel();

      // The search-as-you-type sessions/subject textfield
      this.__searchSubjectSessions = new prearchiveui.SearchTextfieldModel(getSessionSubjects,
									   "Type session or subject name here",
									   1000);


      // The upload date search fields
      this.__searchUploadDate = new prearchiveui.DateRangeModel();
      // The scan date search fields
      this.__searchScanDate = new prearchiveui.DateRangeModel();

      this.__searchBox = new prearchiveui.SearchBoxView(this.__searchUploadDate.getView(),
	                                                this.__searchScanDate.getView(),
							this.__searchProjects.getView(),
							this.__searchStatus.getView(),
							this.__searchSubjectSessions.getView());

      this.__table = new prearchiveui.SessionTableView();
      this.__tableContainer.add(this.__toolbar.create());
      this.__table.create(function () {
                             return that.__sessions.getSessions();
                          }, this.__tableContainer,
			  qx.lang.Function.bind(this.block,this),
			  qx.lang.Function.bind(this.unblock,this));
      this.__addListeners();
      this.__container.add(this.__tableContainer);
    },

    /**
     * Display details about the selected session
     * @param indices{Array} Unique ids
     */
    details : function (indices) {
	  var checkedSessions = this.__sessions.getSessions(this.__table.getChecked());
	  this.__actions.onUpdate(this.__sessions.getSessions(indices),checkedSessions.length);
	  
      if (indices.length == 0) {
	  	var sessions = this.__sessions.getSessions(indices);
	    this.__details.clear();
	  }
	  else if(indices.length == 1){
	  	var sessions = this.__sessions.getSessions(indices);
	    this.__details.onUpdate(sessions[0]);
	  }
	  else if(indices.length > 1){
	    var sessions = this.__sessions.getSessions(indices);
	    this.__details.onUpdateMultiple(sessions[0],indices.length);
      }
      else {
	this.__details.clear();
      }
    },
	
	 /**
     * Display actions for the selected session
     * @param indices{Array} Unique ids
     */
    actions : function (indices) {
      if (indices.length >= 0) {
	var sessions = this.__sessions.getSessions(indices);
	this.__actions.onUpdate(sessions);
      }
      else {
	this.__actions.clear();
      }
    },

    /**
     * Edit or delete one or more sessions.
     * @param actionObj {Object} See prearchiveui.SessionDetails#__initButtonPane for a description of actionObj
     */
    __processAction : function (actionObj) {
      if (actionObj.type == "bulk") {
	if (this.__table.getChecked().length == 0) {
	  new prearchiveui.WarningDialogView("Warning!", "No sessions have been checked!").open();
	}
	else {
	  this.__processBulkAction (actionObj.action);
	}
      }
      else if (actionObj.type === "header") {
	this.__openHeaderWindow(actionObj.archive, actionObj.numDays);
      }
	  else if(actionObj.type === "archive"){
	    this.__processBulkAction(actionObj.action);
	  }
	  else if(actionObj.type === "review"){
	    this.__processSingleArchive(actionObj.action);
	  }
      else { // actionObj.type == "single"
	if (actionObj.id == -1) {
	  new prearchiveui.WarningDialogView("Warning!", "Please select a session!").open();
	}
	else {
	  this.__processSingleAction(actionObj.action, actionObj.id);
	}
      }
    },
	
	 /**
     * Edit or delete one or more sessions.
     * @param actionObj {Object} See prearchiveui.SessionDetails#__initButtonPane for a description of actionObj
     */
    __displayStatus : function (actionObj) {

		if (actionObj.id == -1) {
		  new prearchiveui.WarningDialogView("Warning!", "Please select a session!").open();
		}
		else {
	
  			var restReq = new qx.io.rest.Resource();
			restReq.map("get", "GET", serverRoot+"/data{url}/logs/last");
			var restRes = restReq.get({url: actionObj.url});
						
			restReq.addListener("getSuccess", function(e) {
			var displayMessage = e.__data;
			if(displayMessage==null || displayMessage==""){
				displayMessage="There are no additional details for this session.";
			}
		  	var win = new prearchiveui.DialogView("Last Log",
							  ""+displayMessage);
			win.open();
			});
		  
		}
    },
      
    __openHeaderWindow : function (archiveType, numDays) {
      var that = this;
      var openHeaderView = function (numDays) {
	var window = new qx.ui.window.Window("Loading DICOM headers (" + archiveType + ", " + numDays + " days)");;
	window.setModal(true);
	window.setAllowMinimize(false);
	window.setAllowMaximize(false);
	window.setMovable(true);
	window.setLayout(new qx.ui.layout.VBox());
	window.moveTo(250,250);
	window.setWidth(350);
	var root = that.__serverRoot;
	new dicomheaderview.DicomHeaderView(root,
					    window,
					    {
					      open : function (win) {win.open();},
					      event : null
					    },
					    {
					      close : function (win) {win.close();},
					      event : "close"
					    },
					    archiveType, numDays);
      };
      if (numDays === -1) {
	this.debug("here");
        var win = new prearchiveui.HeaderConfirmationDialog (
	      "DICOM Headers",
	      "View DICOM Headers",
	      openHeaderView,
	      null,
	      100);
	    win.open();
      }
      else { 
	openHeaderView(numDays);
      }
    },

    /**
     * Bulk actions take the selected sessions and return updated sessions.
     * This function syncs the two.
     * @param selectedSessions{Array} The selected (checked in the case of bulk actions) sessions in the table.
     * @param respSessions{Array} The updated sessions.
     * @param keep{Boolean} If true replace the selected sessions with the corresponding sessions in the response,
     *       otherwise delete the selected sessions that match the corresponding sessions in the response
     */
    _syncResponseWithTable : function (selectedSessions, keep, respSessions) {
      if (keep == null) {
	keep = true;
      }
      var that = this;
      var context = {
	current : 0
      };
      /**
       * Find the given session in the list of responses
       * @param sess{Object} The session to be matched
       * @param sessions{Array} The list of sessions to be searched
       */
      var findInResponse = function (sess, sessions) {
	for (var i = 0; i < sessions.length; i++) {
	if (sess.url == sessions[i].url) {
	    return sessions[i];
	  }
	}
	return null;
      };
      // block the main container
      var block = qx.lang.Function.bind(this.block, this);
      // unblock the main container
      var unblock = qx.lang.Function.bind(this.unblock,this);

      /**
       * Do something with the given session if it is
       * in the list of responses
       * @param session{Object} The selected session from the table
       */
      var syncSession = function(session) {
	var sess = findInResponse(session, respSessions);
	  /**
	 * Delete this session from the table
	 * and backing store
	 */
	var deleteSession = function () {
	  // remove from table
	  that.__table.deleteRow(session.unique_id);
	  // remove from store
	  that.__sessions.removeSession(session.unique_id);
	  // recalculate filters
	  that.__recalculateFilters(false);
	  // remove from details
	  that.__details.clear();
	  that.__actions.clear();
	};
	/**
	 * Replace this session with the one in the
	 * response
	 */
	var replaceSession = function () {
	  that.__sessions.replaceSession(session.unique_id, sess);
	  that.__recalculateFilters(false);
	  var newSession = that.__sessions.getSessions([session.unique_id])[0];
	  that.__table.onUpdate(newSession);
	};

	sess != null ? (keep ? replaceSession() : deleteSession()) : deleteSession();
	};
      var func = function (context) {
	if (context.current < selectedSessions.length) {
	  if (qx.lang.Type.isString(respSessions)) {
	    that.__sessions.okSession(selectedSessions[context.current].unique_id);
	    that.__table.onUpdate(selectedSessions[context.current]);
	  }
	  else {
              syncSession(selectedSessions[context.current]);
	  }
	  context.current++;
	}
      };
      prearchiveui.Utils.addProgressive(func,selectedSessions.length,200,this.__container, context,block,unblock);
    },

    /**
     * Edit or delete multiple sessions.
     * @param  action {Object} See prearchiveui.SessionDetails#__initButtonPane for a description of action
     */
    __processBulkAction : function (action) {
      var eligible = {
      "Archive" : prearchiveui.Sessions.ARCHIVABLE,
	  "Review" : prearchiveui.Sessions.ARCHIVABLE,
      "Move" : prearchiveui.Sessions.MOVABLE,
      "Delete" : prearchiveui.Sessions.DELETABLE,
      "Reset" : prearchiveui.Sessions.RESETABLE
      };

      var that = this;
      var checkedSessions = this.__sessions.getSessions(this.__table.getChecked());
      var eligibleSessions = [];
      for (var i = 0; i < checkedSessions.length ; i++) {
	if (eligible[action](checkedSessions[i])) {
	  eligibleSessions.push(checkedSessions[i]);
	}
      }

      /**
       * Ask the server to do a bulk action. Bulk actions that use this function
       * always give the server a "src" attribute for each session.
       * @param url{String} The REST url of the bulk action
       */
      var serverBulkRequest = function (url, keep) {
	  new prearchiveui.ServerRequest(url,
					 that,
					 "csv",
					 {
					   "src" : eligibleSessions.map(function (s) {
									 return s.url;
								       })
					 },
					 qx.lang.Function.curry(that._syncResponseWithTable, eligibleSessions, keep),
					 "POST").request();
					   };
      var archiveAction = qx.lang.Function.curry(serverBulkRequest, "/REST/services/archive", true);
      var deleteAction = qx.lang.Function.curry(serverBulkRequest, "/REST/services/prearchive/delete", true);
	  var resetAction = qx.lang.Function.curry(serverBulkRequest, "/REST/services/prearchive/rebuild", true);
      /*
       * Archive multiple sessions
       */
      if (action == "Archive") {
	if (eligibleSessions.length == 0 ) {
	  new prearchiveui.WarningDialogView("Warning!", "No checked sessions can be archived!").open();
	}
	else {
	  var win = new prearchiveui.ConfirmationDialogView("Confirm" ,
	                                                  "Are you sure you want to archive " + eligibleSessions.length + "/" + checkedSessions.length + " sessions?",
							  archiveAction);
	  win.open();
	}
      }
      /*
       * Delete multiple sessions
       */
      else if (action == "Delete") {
	if (eligibleSessions.length == 0) {
	  new prearchiveui.WarningDialogView("Warning!", "No checked sessions can be deleted!").open();
	}
	else {
	  var win = new prearchiveui.ConfirmationDialogView("Confirm" ,
							  "Are you sure you want to delete " + eligibleSessions.length + "/" + checkedSessions.length + " sessions?",
							    deleteAction);
	  win.open();
	}
      }
	   /*
       * Rebuild multiple sessions
       */
      else if (action == "Reset") {
	if (eligibleSessions.length == 0) {
	  new prearchiveui.WarningDialogView("Warning!", "No checked sessions can be rebuilt!").open();
	}
	else {
	  var intermediateSession = false;
	  for(var sessCounter=0;sessCounter<eligibleSessions.length;sessCounter++){
	    if((eligibleSessions[sessCounter].status == "ARCHIVING" || eligibleSessions[sessCounter].status == "QUEUED_ARCHIVING" || eligibleSessions[sessCounter].status == "BUILDING" || eligibleSessions[sessCounter].status == "QUEUED" || eligibleSessions[sessCounter].status == "QUEUED_BUILDING" || eligibleSessions[sessCounter].status == "DELETING" || eligibleSessions[sessCounter].status == "MOVING" || eligibleSessions[sessCounter].status == "QUEUED_MOVING" || eligibleSessions[sessCounter].status == "QUEUED_DELETING" || eligibleSessions[sessCounter].status == "RECEIVING")){
		  intermediateSession = true;  
		}
	  }
	  if(intermediateSession){
	    var win = new prearchiveui.ConfirmationDialogView("Rebuild" ,
							  "Are you sure you want to rebuild " + eligibleSessions.length + "/" + checkedSessions.length + " sessions? One of these sessions may be in the middle of processing.",
							    resetAction);
	  }
	  else{
	    var win = new prearchiveui.ConfirmationDialogView("Rebuild" ,
							  "Are you sure you want to rebuild " + eligibleSessions.length + "/" + checkedSessions.length + " sessions?",
							    resetAction);
	  }
	  win.open();
	}
      }
      /*
       * Move multiple sessions to another project
       */
      else if (action == "Move") {
	var moveAction = function (proj) {
	  new prearchiveui.ServerRequest("/REST/services/prearchive/move",
					 that,
					 "csv",
					 {
					   "src" : eligibleSessions.map(function (s) {
								      return s.url;
								    }),
					   "newProject" : proj
					 },
					 qx.lang.Function.curry(that._syncResponseWithTable, eligibleSessions, true),
					 "POST").request();

	};
	var conf = function (projs) {
	  if (eligibleSessions.length == 0) {
	    new prearchiveui.WarningDialogView("Warning!", "No checked sessions can be moved!").open();
	  }
	  else {
	    var win = new prearchiveui.MoveConfirmationDialogView (
	      "Move",
	      "Moving " + eligibleSessions.length + "/" + checkedSessions.length + " sessions",
	      moveAction,
	      null,
	      qx.lang.Array.unique(qx.lang.Array.append(projs,that.__sessions.getNonUnassignedProjects())));
	    win.open();
	  }
	};

	// make the request
	var req = new prearchiveui.ServerRequest("/REST/projects",
						 this,
						 "csv",
						 {"data" : "writable", "columns" : "id"},
						 function (data) {
						   var projs = [];
						   for (var i = 0; i < data.length; i++) {
						     projs.push(data[i]["id"]);
						   }
						   conf(projs);
						 });
	req.request();
      }
      else {
	throw new Error("__processBulkAction : " + action + " is not a recognized action");
      }
    },

	    /**
     * Archive one session
     * @param  action {Object} See prearchiveui.SessionDetails#__initButtonPane for a description of action
     * @param uniqueSessionId{Int} Id of session on which to perform this action.
     */
    __processSingleArchive : function () {
      var checkedSessions = this.__sessions.getSessions(this.__table.getChecked());
      var eligibleSessions = [];
      for (var i = 0; i < checkedSessions.length ; i++) {
		if (prearchiveui.Sessions.ARCHIVABLE(checkedSessions[i])) {
		  eligibleSessions.push(checkedSessions[i]);
		}
      }
	  
	  if (eligibleSessions.length == 0 ) {
		new prearchiveui.WarningDialogView("Warning!", "No checked sessions can be archived!").open();
	  }
	  else if (eligibleSessions.length > 1 ) {
		new prearchiveui.WarningDialogView("Warning!", "You may only review one session at a time. Please uncheck the other sessions.").open();
	  }
	  else{
		  var session = eligibleSessions[0];
			  
		  /*
		   * Archive a single session
		   */
		var redirect  = this.__serverRoot
		  + "/app/action/LoadImageData/project/"
		  + session.project
		  + "/timestamp/"
		  + session.timestamp
		  + "/folder/"
		  + session.folderName
			  + "/popup/false/purpose/insert";
		window.location=redirect;
	}
    },
	
    /**
     * Edit or delete one session
     * @param  action {Object} See prearchiveui.SessionDetails#__initButtonPane for a description of action
     * @param uniqueSessionId{Int} Id of session on which to perform this action.
     */
    __processSingleAction : function (action, uniqueSessionId) {
      var that = this;
      var session = this.__sessions.getSessions([uniqueSessionId])[0];
      var deleteSession = qx.lang.Function.curry(this._syncResponseWithTable, [session], false);
      var resetSession = qx.lang.Function.curry(this._syncResponseWithTable, [session], true);
      var deleteAction = function () {
	  new prearchiveui.ServerRequest("/REST/services/prearchive/delete",
						   that,
						   "csv",
						   {"src" : session.url, "async" : false},
						   deleteSession,
						   "POST").request();

      };

      var resetAction = function () {
    new prearchiveui.ServerRequest("/REST" + session.url,
					 that,
					 "single",
					 {"action" : "build"},
					 resetSession,
					 "POST").request();
					 };
    if (action == "Delete") {
	var win = new prearchiveui.ConfirmationDialogView("Delete" ,
							  "Are you sure you want to delete this session?",
							  deleteAction);
	win.open();
      }

      else if (action == "Reset") {
		if((session.status == "ARCHIVING" || session.status == "QUEUED_ARCHIVING" || session.status == "BUILDING" || session.status == "QUEUED" || session.status == "QUEUED_BUILDING" || session.status == "DELETING" || session.status == "MOVING" || session.status == "QUEUED_MOVING" || session.status == "QUEUED_DELETING" || session.status == "RECEIVING")){
			var win = new prearchiveui.ConfirmationDialogView("Reset",
							  "Are you sure you want to rebuild this session? Rebuilding a session while in the middle of another operation is not recommended.",
							  resetAction);
		}
		else{
			var win = new prearchiveui.ConfirmationDialogView("Reset",
							  "Are you sure you want to rebuild this session?",
							  resetAction);
		}
	win.open();
      }

      /*
       * Move a session to another project
       */
	else if (action == "Move") {
	// move session and update table
	var moveAction = function (proj) {
	  var postServerAction = function () {
	    that.__sessions.moveSession(uniqueSessionId, proj);
	    that.__recalculateFilters(false);
	    var updatedSess = that.__sessions.getSessions([uniqueSessionId])[0];
	    that.__table.onUpdate(updatedSess);
	  };
	  new prearchiveui.ServerRequest("/REST/services/prearchive/move",
					 that,
					 "csv",
					 {
					   "src" : session.url,
					   "newProject" : proj,
					   "async" : "false"
					 },
					 postServerAction,
					 "POST"
					).request();
	};
	// confirm move and get the new project
	var conf = function (projs) {
	  var win = new prearchiveui.MoveConfirmationDialogView (
	    "Move",
	    "Moving 1 session",
	    moveAction,
	    null,
	    qx.lang.Array.unique(qx.lang.Array.append(projs,that.__sessions.getNonUnassignedProjects())));
	  win.open();
	};
	// make the request
	var req = new prearchiveui.ServerRequest("/REST/projects",
						 this,
						 "csv",
						 {"data" : "writable", "columns" : "id"},
						 function (data) {
						   var projs = [];
						   for (var i = 0; i < data.length; i++) {
						     projs.push(data[i]["id"]);
						   }
						   conf(projs);
						 });
	req.request();
      }
	  
	  
	  	  	
	    /*
       * Display a session's status
       */
      else if (action == "Status") {
	  
	  
	  		var restReq = new qx.io.rest.Resource();
			restReq.map("get", "GET", serverRoot+"/session{url}/logs/last");
			var restRes = restReq.get({url: url});
						
			restReq.addListener("getSuccess", function(e) {
		  	var win = new prearchiveui.ArchiveDialogView("Reason for Status",
							  ""+e.__data);
			win.open();
			});
			
			
				  			var restReq = new qx.io.rest.Resource();
			restReq.map("get", "GET", serverRoot+"/data{url}/logs/last");
			var restRes = restReq.get({url: data.url});
						
			restReq.addListener("getSuccess", function(e) {
		  	var win = new prearchiveui.ArchiveDialogView("Archive",
							  "Do you want to review this session before archiving?",
							  reviewAction,archiveAction);
			win.open();
			});
	  
      }
      else {
	throw new Error("__processSingleAction : " + action + " is not a recognized action");
      }
    }
  }
});
