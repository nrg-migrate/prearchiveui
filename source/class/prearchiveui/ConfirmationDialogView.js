/**
 * Creates a dialog that confirms some action
 */
qx.Class.define("prearchiveui.ConfirmationDialogView",
{
  extend : qx.core.Object,
  /**
   * @param title{String} The title of the dialog window.
   * @param message{String} The message in the dialog window.
   * @param okAction{Function} Run if the user confirms
   * @param cancelAction{Function} Run if the user cancels.
   */
  construct : function (title, message, okAction, cancelAction) {
    this.base(arguments);
    this.setTitle(title);
    this.setMessage(message);
    this.initOkAction(okAction != null ? okAction : prearchiveui.Utils.dummy);
    this.initCancelAction(cancelAction != null ? cancelAction : prearchiveui.Utils.dummy);
    this._init();
  },
  properties : {
    /**
     * See constructor comment for explanation on this property.
     */
    message : {
      check : "String"
    },
    /**
     * This dialog window
     */
    window : {
      check : "qx.ui.window.Window",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    title : {
      check : "String",
      init : ""
    },
    /**
     * The number of sessions on which this action will be performed.
     */
    numSessions : {
      check : "Number",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    okAction : {
      check : "Function",
      deferredInit : true
    },
    /**
     * See constructor comment for explanation on this property.
     */
    cancelAction : {
      check : "Function",
      deferredInit : true
    }
  },
  members : {
    _initWindow : function () {
      this.initWindow(new qx.ui.window.Window(this.getTitle()));
      this.getWindow().setModal(true);
      this.getWindow().setAllowMinimize(false);
      this.getWindow().setAllowMaximize(false);
      this.getWindow().setMovable(false);
      this.getWindow().setLayout(new qx.ui.layout.VBox());
      this.getWindow().moveTo(250,250);
    },
    _initMessage : function () {
      this.getWindow().add(new qx.ui.basic.Atom(this.getMessage()));
    },

    _init : function () {
      this._initWindow();
      this._initMessage();
      this._initButtons();
    },

    _initButtons : function () {
      var box = new qx.ui.container.Composite;
      box.setLayout(new qx.ui.layout.HBox(10, "right"));
      this.getWindow().add(box);

      var ok = new qx.ui.form.Button("Ok", "icon/16/actions/dialog-ok.png");
      var cancel = new qx.ui.form.Button("Cancel", "icon/16/actions/dialog-cancel.png");
      this._initListeners(ok,cancel);

      box.add(ok);
      box.add(cancel);
    },

    _initListeners : function (ok,cancel) {
      ok.addListener("execute", function(e) {
		       this.getWindow().close();
		       this.getOkAction()();
		     },this);
      cancel.addListener("execute", function(e) {
			   this.getWindow().close();
			   this.getCancelAction()();
			 },this);
    },

    /**
     * Dpen this dialog window.
     */
    open : function () {
      this.getWindow().open();
    }
  }
});
