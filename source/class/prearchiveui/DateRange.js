qx.Class.define("prearchiveui.DateRange",
{
  extend : qx.core.Object,
  properties : {
    first : {
      check : "Date",
      init : new Date("Dec 31 00:00:00 CST 1800")
    },
    second : {
      check : "Date",
      init : new Date("Dec 31 00:00:00 CST 3000")
    },
    valid : {
      check : "Boolean",
      init : false
    }
  },
  members : {
    addDate : function (first, date) {
      if (first) {
        if (date == null) {
	  this.resetFirst();
	}
	else {
	  if (date > this.getSecond()){
	    throw new qx.core.ValidationError
            ( "Validation Error : first date : " + date + " must be less than the second date : " + this.getSecond());;
	  }
	  else {
	    this.setFirst(date);
	  }
	}
      }
      else {
	if (date == null) {
	  this.resetSecond();
	}
	else {
	  if (date < this.getFirst()){
	    throw new qx.core.ValidationError
	    ( "Validation Error : second date : " + date + " must be greater than the first date : " + this.getFirst());;
	  }
	  else {
	    this.setSecond(date);
	  }
	}
      }
      this.setValid(true);
    },
    reset : function () {
      this.resetFirst();
      this.resetSecond();
      this.resetValid(false);
    }
  }
});
