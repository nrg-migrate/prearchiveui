/**
 * <pre>
 * This class is a database of sessions received from the XNAT backend. It is the 'model' in 'model-view-controller'.
 * It should instantiated once and never directly by the user but by the controller.
 *
 * ============================
 * Session Data Characteristics
 * ============================
 *
 * Between 500 and 50,000 total sessions are expected to be in the prearchive.
 *
 * In terms of RAM consumption a mock JSON object containing 50,000 sessions containing the project name, session name,
 * session id, upload date, and session time weighed in at slightly under 9MB.
 *
 * It is also assumed that project names are unique but session labels are not. For instance both projects 'proj1'
 * and 'proj2' might have two different 'sess1' sessions.
 *
 * ==============================================
 * Server Representation of Sessions And Projects
 * ==============================================
 * See the comments for the __makeInternalRepresentation function.
 *
 * ================================================
 * Internal Representation of Sessions And Projects
 * ================================================
 * All data received from the server is stored in a data structure where each session is has a unique internal
 * id and is part of an array referenced by individual projects. See Fig. 1 below.
 *
 * Project Store
 * =============
 * The project is currently implemented as a large object where each project name
 * is a property of the large object. Eg.
 * {
 *   'proj1' : [sessions_for_proj1],
 *   'proj2' : [sessions_for_proj2],
 *    ....
 *   'projN' : [sessions_for_projN]
 * }
 *
 * This makes referencing a project easy (eg. __projStore['proj1]) but since it isn't an
 * array finding the number of projects involves counting the properties of the large project
 * object. Since we expect that there will be many less projects than sessions this shouldn't
 * be an expensive operation.
 *
 * Session Store
 * =============
 * Session store is an array in which each session is assigned a internal id.
 *
 * Session Data Structure
 * ======================
 * Each session is an object containing information sent from the server. Additionally
 * it also holds its internal id and the name of the project to which it belongs. Eg.
 * {
 *   'unique_id : ...,
 *   'subject' : ...
 *   'session' : ...
 *   'project' : ...
 * }
 *
 *
 * Figure 1. Internal Representation of Sessions and Projects
 * -----------------------------------------------------------
 * __projStore                                             __sessStore
 * ------------                                            ------------
 *                          +------------------------------------+
 *                          v                                    |
 * +------------+     +-----+-------+---------------+     +------------+
 * |  proj_1    |---->|  sess_1     |    sess_2     |<+   | id_1       |
 * |            |     +-------------+---------------+ |   +------------+
 * +------------+     +--------+----------+---------+ +---| id_2       |
 * |  proj_2    |---->|  sess_3| sess_4   | sess_5  |<-+  +------------+
 * |            |     +--------+----+-----+---------+ +|--| id_3       |
 * +------------+         ^         ^                 ||  +------------+
 * |  .....     |         +---------|-----------------+|+-| id_4       |
 * |            |                   +------------------++ +------------+
 * +------------+     +-------------+---------------+  +- | id_5       |
 * |  proj_n    |---->| sess_n-2    | sess_n-1      |     +------------+
 * +------------+     +----+--------+------+--------+          |
 *                         ^               ^                  ...
 *                         |               |                   |
 *                         |               |                   v
 *                         |               |              +------------+
 *                         |---------------|--------------| id_n-2     |
 *                                         |              +------------+
 *                                         ---------------| id_n-1     |
 *                                                        +------------+
 *</pre>
 */
qx.Class.define("prearchiveui.Sessions",
{
  extend : qx.core.Object,
  /**
   * @param url{String} The server url which responds with a list of sessions
   * @param parser{String} The parser used to convert the server response into internal objects. See prearchiveui.ServerRequest.formatParserMap for the supported values.
   */
  construct : function (url, parser) {
    this.base(arguments);
    this.__init_store(url,parser);
  },
  statics : {
    /**
     * Test if the given session is movable.
     * @param session{Object} The given session.
     */
    MOVABLE : function (session) {
      return !prearchiveui.Utils.isLocked(session) && session.status != "QUEUED_MOVING" && session.status != "QUEUED_DELETING" && session.status != "MOVING" && session.status != "RECEIVING" && session.status != "ERROR" && session.status != "ARCHIVING" && session.status != "QUEUED_ARCHIVING" && session.status != "BUILDING" && session.status != "QUEUED" && session.status != "QUEUED_BUILDING" && session.status != "DELETING";
    },
    /**
     * Test if the given session is archivable.
     * @param session{Object} The given session.
     */
    ARCHIVABLE : function (session) {
	  return !prearchiveui.Utils.isLocked(session) && session.status != "QUEUED_MOVING" && session.status != "QUEUED_DELETING" && (session.status == "READY" || session.status == "CONFLICT") && session.project != prearchiveui.Utils.UNASSIGNED;
    },
    /**
     * Test if the given session is deletable.
     * @param session{Object} The given session.
     */
    DELETABLE : function (session) {
      return !prearchiveui.Utils.isLocked(session) && session.status != "QUEUED_MOVING" && session.status != "QUEUED_DELETING" && session.status != "ARCHIVING" && session.status != "QUEUED_ARCHIVING" && session.status != "BUILDING" && session.status != "QUEUED" && session.status != "QUEUED_BUILDING" && session.status != "DELETING" && session.status != "MOVING";
    },
  /**
     * Test if the given session is resetable.
     * @param session{Object} The given session.
     */
    RESETABLE : function (session) {
      return ((document.getElementById("role").title==="admin") || (!prearchiveui.Utils.isLocked(session) && session.status != "QUEUED_MOVING" && session.status != "QUEUED_DELETING" && session.status != "ARCHIVING" && session.status != "QUEUED_ARCHIVING" && session.status != "BUILDING" && session.status != "QUEUED" && session.status != "QUEUED_BUILDING" && session.status != "DELETING" && session.status != "MOVING"));
    }
  },
  events : {
    /**
     * See prearchiveui.SessionController for event description
     */
    "loaded" : "qx.event.type.Event"
  },
  members : {
    __sessStore : null,
    __projStore : null,

    /**
     * Ask the server for the prearchive information, post-process and populate the 'store' property
     * and fire the "loaded" event on completion.
     * @param url {String} The *relative* url to query
     * @param parser {String} The parser used to parse the results.
	 * @lint ignoreDeprecated(alert)
     */
    __init_store : function (url,parser) {
      var transformer = function (sessions) {
	this.__makeInternalRepresentation(sessions);
      };
      var req = null;

      req = new prearchiveui.ServerRequest(url,
					   this,
					   "csv",
					   null,
					   transformer,
					   null,
					   null,
					   function (){
					     prearchiveui.Utils.showNotification();
					     prearchiveui.Utils.notify("Requesting data from server");
					   },
					   function () {
					     prearchiveui.Utils.notify("Receiving data from server");
					   },
					   function () {
					     prearchiveui.Utils.hideNotification();
					   },
					   function (str) {
					     alert(str);
					   });
      // if the browser is IE parse the CSV progressively
      if (qx.bom.client.Browser.getName() == 'ie')  {
	req.progressive(true);
      }
      else {
	req.progressive(false);
      }
      req.request();
    },
    /**
     * Post-process the data received from the server into the internal
     * data structure described in the header comment.
     * The expected structure of data returned from the server is:
     * ResultSet : [
     *     Result : { lastmod: "2010-11-29 13:30:44.0",
     *	              name: "F063TE",
     *	              project: "Test2",
     *	              scan_date: "2006-10-19 00:00:00.0",
     *                scan_time: "14:37:39",
     *	              status: "READY",
     *	              subject: "" ,
     *	              timestamp: "20101129_133041",
     *	              uploaded: "2010-11-29 13:30:41.0",
     *	              url: "/tmp/PRE_ARCHIVE_LOCATION/Test2/20101129_133041/F063TE"
     *	            }
     *            ...
     *        ]
     * This function should only be called once.
     * @param _sessions{Array} Sessions retrieved from the server and parsed
     */
    __makeInternalRepresentation : function (_sessions) {
      var _projects = {};
      for (var i = 0; i < _sessions.length; i++){
	_sessions[i].unique_id = i;
	_sessions[i].lastmod = this.__parseDate(_sessions[i].lastmod);
	_sessions[i].uploaded = this.__parseDate(_sessions[i].uploaded);
				  _sessions[i].scan_date = this.__parseDate(_sessions[i].scan_date);
				    var name = _sessions[i].project;
        if (_projects[name] == null) {
      _projects[name] = [];
	}
	  _projects[name].push(_sessions[i]);
      }
      this.__sessStore = _sessions;
      this.__projStore = _projects;
    },
      
    /**
     * qx.util.format.DateFormat.parse is too slow for IE. This ugly hack scans the
     * date string returned from the server manually.
     *
     * The format of the date string returned from the server is expected to be, eg.
     * "2010-11-29 13:32:01.0" arranged by array index is:
     * <pre>
     *
     * | Index | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 |
     * |-------+---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----|
     * | Value | 2 | 0 | 1 | 0 | - | 1 | 1 | - | 2 | 9 |    |  1 |  3 |  : |  3 |  2 |  : |  0 |  1 |  . |  0 |
     * </pre>
     * @param str {String} The date string to be parsed
     */
    __parseDate : function (str) {
      if (str != null && str.length > 0) {
	var date = new Date();
	var year	= parseInt(str.substring(0,4),10);
	// months in the Date object are 0-11 not 1-12, hence the ' - 1'
	var month	= parseInt(str.substring(5,7),10) - 1;
	var day		= parseInt(str.substring(8,10),10);
	var hour	= parseInt(str.substring(11,13),10);
	var minute	= parseInt(str.substring(14,16),10);
	var second	= parseInt(str.substring(17,19),10);

	date.setFullYear(year,month,day);
	date.setHours(hour,minute,second);
	return date;
      }
      else {
	return null;
      }
    },

    //////////////////////////////////////////////////////////////////////////////////////
    // Store Accessor/Mutator Functions					     	        //
    // ================================						        //
    // Below are functions that manipulate the sessions and project store.	        //
    // They are categorized into functions that add, edit, remove or search    	        //
    // either sessions or projects					     	        //
    //////////////////////////////////////////////////////////////////////////////////////

    ///////////////
    // Searching //
    ///////////////

    /**
     * Returns ids of the sessions in the given projects
     * @param projects{Array} a list of project names
     */
    getSessionsInProjects : function (projects) {
      var ids = [];
      for (var proj = 0 ; proj < projects.length; proj++){
		if (this.__projStore[projects[proj]]) {
		  for (var sess = 0; sess < this.__projStore[projects[proj]].length ; sess++){
			ids.push(this.__projStore[projects[proj]][sess].unique_id);
		  }
		}
      }
      return ids;
    },

    /**
     * Returns ids of sessions uploaded between certain dates
     * @param begin{Date} Start of date range
     * @param end{Date} end of date range
     */

    getSessionsUploadedBetween : function (begin, end) {
      var ids = [];
      for (var i =0; i < this.__sessStore.length; i++){
		var sess = this.__sessStore[i];
		if(!(sess.uploaded == null)){
			var afterBeginDate = 0;
			var beforeEndDate = 0;
			var uploadedDate = new Date(sess.uploaded.getTime()).setHours(0,0,0,0);
			if(!(begin == null)){
				var beginDate = new Date(begin.getTime()).setHours(0,0,0,0);
				if (uploadedDate >= beginDate){
					afterBeginDate = 1;
				}
			}
			else{
				afterBeginDate = 1;
			}
			if(!(end == null)){
				var endDate = new Date(end.getTime()).setHours(0,0,0,0);
				if (uploadedDate <= endDate){
					beforeEndDate = 1;
				}
			}
			else{
				beforeEndDate = 1;
			}
			
			if(afterBeginDate && beforeEndDate){
			  ids.push(sess.unique_id);
			}
		}
		else{
			if((begin == null) && (end == null)){
				ids.push(sess.unique_id);
			}
		}
      }
      return ids;
    },


    /**
     * Returns ids of sessions scanned between certain dates
     * @param begin {Date} Start of date range
     * @param end {Date} End of date range
     */

    getSessionsScannedBetween : function (begin,end) {
      var ids = [];
      for (var i = 0; i < this.__sessStore.length; i++) {
		var sess = this.__sessStore[i];

		if(!(sess.scan_date == null)){
			var afterBeginDate = 0;
			var beforeEndDate = 0;
			var scannedDate = new Date(sess.scan_date.getTime()).setHours(0,0,0,0);
			if(!(begin == null)){
				var beginDate = new Date(begin.getTime()).setHours(0,0,0,0);
				if (scannedDate >= beginDate){
					afterBeginDate = 1;
				}
			}
			else{
				afterBeginDate = 1;
			}
			if(!(end == null)){
				var endDate = new Date(end.getTime()).setHours(0,0,0,0);
				if (scannedDate <= endDate){
					beforeEndDate = 1;
				}
			}
			else{
				beforeEndDate = 1;
			}
			
			if(afterBeginDate && beforeEndDate){
			  ids.push(sess.unique_id);
			}
		}
		else{
			if((begin == null) && (end == null)){
				ids.push(sess.unique_id);
			}
		}
      }
      return ids;
    },


    /**
     * Returns ids of sessions where either the subject or session match the
     * given names,
     * @param subjOrSessions {String} Name of subject or session
     */

    getSessionsBySubjectOrSession : function (subjOrSessions) {
      var ids = [];
      for (var i = 0; i < this.__sessStore.length; i++) {
	var sess = this.__sessStore[i];
	var found = false;
	for (var j = 0; j < subjOrSessions.length; j++) {
	  if (sess.subject == subjOrSessions[j] || sess.name == subjOrSessions[j]) {
	    found = true;
	  }
	}
	if (found) {
	  ids.push(sess.unique_id);
	}
      }
      return ids;
    },

    /**
     * Return ids of sessions where the statuses match the given status
     * @param status {String} Status string on which to match
     */
    getSessionsWithStatus : function (status) {
      var ret = [];
      if (status != "") {
		  var statusToDisplay = "Unknown Status";
		  if(status == "Build pending"){
			ret = this.__sessStore.filter(function (sess) {
					return (sess.status == "BUILDING" || sess.status=="QUEUED" || sess.status=="_QUEUED" || sess.status=="QUEUED_BUILDING");
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  else if(status == "Receiving"){
			ret = this.__sessStore.filter(function (sess) {
					return (sess.status == "RECEIVING" || sess.status=="_RECEIVING");
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  else if (status == "Conflict"){
			ret = this.__sessStore.filter(function (sess) {
					return (sess.status == "CONFLICT" || sess.status=="_CONFLICT");
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  else if (status == "Archive pending"){
			ret = this.__sessStore.filter(function (sess) {
					return (sess.status == "ARCHIVING" || sess.status=="QUEUED_ARCHIVING");
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  else if (status == "Delete pending"){
			ret = this.__sessStore.filter(function (sess) {
					return (sess.status == "DELETING" || sess.status=="QUEUED_DELETING");
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  else if (status == "Move pending"){
			ret = this.__sessStore.filter(function (sess) {
					return (sess.status == "MOVING" || sess.status=="QUEUED_MOVING");
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  else{
		  var internalStatus = "";
			  if (status=="Building now" ){
				internalStatus = "_BUILDING";
			  }
			  else if (status=="Archiving now" ){
				internalStatus = "_ARCHIVING";
			  }
			  else if (status=="Moving now" ){
				internalStatus = "_MOVING";
			  }
			  else if (status=="Deleting now" ){
				internalStatus = "_DELETING";
			  }
			  else if (status=="Ready" ){
				internalStatus = "READY";
			  }
			  else if (status=="Error"){
				internalStatus = "ERROR";
			  }		  
				ret = this.__sessStore.filter(function (sess) {
					return sess.status == internalStatus;
				      }).map(function (sess) {
						return sess.unique_id;
					      });
		  }
		  
      }
      return ret;
    },


    ////////////////
    // Editing    //
    ////////////////

    /**
     * Make the session matching the given unique id READY
     * @param unique_id {Int} Session identifier
     */
    okSession : function (unique_id) {
      var that = this;
      this.__withSession(unique_id, function (old) {
			   old.status = "READY";
			 });
    },

    /**
     * Replace a session
     * @param unique_id{Number} Sessions unique identifier
     * @param session{Object} The session to which the given identifier should point.
     */
    replaceSession : function (unique_id, session) {
      var that = this;
      this.__withSession(unique_id, function (old) {
			   session.unique_id = unique_id;
			   session.lastmod = that.__parseDate(session.lastmod);
			   session.uploaded = that.__parseDate(session.uploaded);
			   session.scan_date = that.__parseDate(session.scan_date);
			   that.__sessStore[unique_id] = session;
			   if (old.project != session.project) {
			     that.moveSession(unique_id, session.project);
			   }
			 });
    },

    /**
     * Change the name of a session
     * @param unique_id{Object} Sessions unique identifier
     * @param new_name{String} This sessions new name
     */
    renameSession : function (unique_id, new_name) {
      this.__withSession(unique_id, function (sess) {
			   sess.name = new_name;
			 });
    },

    /**
     * Move the given session to the existing project
     * @param unique_id{Object} Sessions unique identifier
     * @param proj_name{String} This session's new project
     */
    moveSession : function (unique_id, proj_name) {
      var that = this;
	this.__withSession(unique_id, function (sess) {
			     that.__removeSessionFromProject(unique_id, sess.project);
			     if (that.__projStore[proj_name] == null){
			       that.addSessionToNewProject(unique_id,proj_name);
			     }
			     else {
			       that.__addSessionToProject(sess,proj_name);
			     }
			});
    },

    /**
     * Move a session to a new project
     * @param unique_id{Object} Sessions unique identifier
     * @param proj_name{String} The new project name
     */
    addSessionToNewProject : function (unique_id, proj_name) {
      var that = this;
      this.__withNewProject(proj_name, function (proj) {
			      that.__withSession(unique_id, function (sess){
						   proj.push(sess);
						   sess.project = proj_name;
						 });
			    });
    },

    ///////////////
    // Deletion  //
    ///////////////

    /**
     * Removes a session from the store.
     * @param unique_id{Object} Sessions unique identifier
     */
    removeSession : function (unique_id) {
      var proj_name = null;
      this.__withSession(unique_id , function (sess){
			   proj_name = sess.project;
			 });
      this.__removeSessionFromProject(unique_id,proj_name);
      this.__removeSession(unique_id);
    },


    /**
     * Remove a session from the session store
     * @param unique_id{Object} Sessions unique identifier
     */
    __removeSession : function (unique_id) {
      this.__sessStore[unique_id] = null;
    },

    /**
     * Remove all references to a session from the project store
     * @param unique_id{Object} Sessions unique identifier
     * @param proj_name{String} Project from which to remove this session
     */
    __removeSessionFromProject : function (unique_id, proj_name){
      this.__withProject(proj_name, function (proj) {
			   for (var i = 0; i < proj.length; i++) {
			     var sess = proj[i];
			     if (sess.unique_id == unique_id){
			       proj.splice(i,1);
			       break;
			     }
			   }
			 });
    },

    ////////////
    // Adding //
    ////////////

    /**
     * Add a session to a project
     * @param sess{Object} The session to be added
     * @param proj_name{String} The project to which to add the given session.
     */

    __addSessionToProject : function (sess, proj_name) {
      this.__withProject(proj_name, function (proj) {
			   sess.project=proj_name;
			   proj.push(sess);
			 });
    },


    /////////////////
    // Accessors   //
    /////////////////

    /**
     * Return all the projects names in the store
     * @param unique_ids{Array} A list of unique session identifiers
     */
    getProjects : function (unique_ids) {
      var ret = [];
      if (unique_ids != null && unique_ids != -1) {
	for (var i = 0; i < unique_ids.length; i++){
	  ret[i] = this.__sessStore[unique_ids[i]].project;
	}
	return qx.lang.Array.unique(ret);
      }
      else {
	for (name in this.__projStore){
	  ret.push(name);
	}
	return ret;
      }
    },

    /**
     * Return all projects except the Unassigned ones.
     */
    getNonUnassignedProjects : function (unique_ids) {
      return this.getProjects().filter(function (r) {
					 return r != prearchiveui.Utils.UNASSIGNED;
				       },this);
    },

    /**
     * Returns all the sessions in the session store. Currently it just
     * returns a *reference* to __sessStore which means that any object that
     * receives it can do whatever it wants to it. This is bad. The only practical solution
     * I can think of is to return a copy of the session store, but that means
     * using extra memory.
     *
     * Since efficiency is of prime concern I am trading off safety here.
     * @param unique_ids{Array} A list of unique session identifiers
     */
    getSessions : function (unique_ids){
      var ret = [];
      if (unique_ids != null && unique_ids != -1) {
	for (var i = 0; i < unique_ids.length; i++){
	  ret[i] = this.__sessStore[unique_ids[i]];
	}
	return ret;
      }
      else {
	return this.__sessStore;
      }
    },

    /**
     * Finds the session in the store indexed by unique_id.
     * @param unique_id{Int} Unique id of the session to match
     */
    __find_entry_by_id : function (unique_id) {
      return this.__sessStore[unique_id];
    },


    /////////////////////////////////////////////////////////////////////////////
    // Functions for working safely with projects and sessions		       //
    // ======================================================		       //
    // Takes a callback and makes sure the project or session is valid before  //
    // calling it.							       //
    /////////////////////////////////////////////////////////////////////////////

    /**
     * Make sure the project does not exist before running the callback.
     * @param proj_name{String} The project name to create
     * @param func{Function} Function to run on the project when it has been created.
     */
    __withNewProject : function (proj_name, func) {
      if (this.__projStore[proj_name] != null) {
	throw ("Project : " + proj_name + "exists!");
      }
      else {
	this.__projStore[proj_name] = [];
	func(this.__projStore[proj_name]);
      }
    },

    /**
     * Make sure the session exists before running the callback
     * @param unique_id{Int} Id of the session
     * @param func{Function} Function to run on the session
     */
    __withSession : function (unique_id, func) {
      var sess = this.__find_entry_by_id(unique_id);
      if (sess != null) {
	func(sess);
      }
      else {
	throw ("Session: " + unique_id + " not found");
      }
    },

    /**
     * Make sure the project exists before running the callback
     * @param proj_name{String} Name of project
     * @param func{Function} Function to run on the project
     */
    __withProject : function (proj_name, func) {
      if (this.__projStore[proj_name] != null) {
	func(this.__projStore[proj_name]);
      }
      else {
	throw ("Project " + proj_name + "  does not exist");
      }
    }
  }
});

