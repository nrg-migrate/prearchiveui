/**
 * A preview pane that displays details about the row selected. Should only be instantiated once by the controller.
 */
qx.Class.define("prearchiveui.SessionDetails",
{
  extend : qx.core.Object,
  construct : function () {
    this.base(arguments);
    this.__unique_id = -1;
  },
  events : {
    /**
     * See prearchiveui.SessionController for explanation on this event.
     */
    "changeAction" : "qx.event.type.Data"
  },
  members : {
    __sessionDetailsStack : null,
    __sessionDetailsContainer : null,
    __blocker : null,
    __sessionDetails : null,
    __unique_id : null,
    __richLabel : function (v) {
      var label = new qx.ui.basic.Label();
      label.setRich(true);
      label.setValue(v);
      return label;
    },
	__translateStatus : function (internalStatus) {
      if(internalStatus=="BUILDING" || internalStatus=="QUEUED" || internalStatus=="_QUEUED" || internalStatus=="QUEUED_BUILDING" ){
		return "Build pending";
	  }
	  else if (internalStatus=="_BUILDING" ){
		return "Building now";
	  }
	  else if (internalStatus=="ARCHIVING" || internalStatus == "QUEUED_ARCHIVING"){
		return "Archive pending";
	  }
	  else if (internalStatus=="_ARCHIVING" ){
		return "Archiving now";
	  }
	  else if (internalStatus=="MOVING" || internalStatus=="QUEUED_MOVING"){
		return "Move pending";
	  }
	  else if (internalStatus=="_MOVING" ){
		return "Moving now";
	  }
	  else if (internalStatus=="DELETING" || internalStatus=="QUEUED_DELETING"){
		return "Delete pending";
	  }
	  else if (internalStatus=="_DELETING" ){
		return "Deleting now";
	  }
	  else if (internalStatus=="RECEIVING" || internalStatus=="_RECEIVING"){
		return "Receiving";
	  }
	  else if (internalStatus=="READY" ){
		return "Ready";
	  }
	  else if (internalStatus=="CONFLICT" || internalStatus=="_CONFLICT"){
		return "Conflict";
	  }
	  else if (internalStatus=="ERROR"){
		return "Error";
	  }
	  else{
	    return "Unknown Status";
	  }
    },

    __addLabel : function (v, r, c) {
      this.__sessionDetailsContainer.add(this.__richLabel(v), {row : r, column : c});
    },
    __addInfo : function (v,r,c) {
      if (v != null) {
	if (this.__sessionDetailsContainer.getLayout().getCellWidget(r,c) != null) {
          this.__sessionDetailsContainer.getLayout().getCellWidget(r,c).setValue(v.toString());
	}
	else {
      	  this.__addLabel(v.toString(),r,c);
	}
      }
    },

    __initSessionDetails : function () {
      var that = this;
      var title = function (v) {
	var label = that.__richLabel(v);
	label.setTextAlign("center");
	return label;
      };
      this.__addLabel("<b>Project :</b>", 0, 0);
	  this.__addLabel("<b>Subject :</b>", 1, 0);
	  this.__addLabel("<b>Session :</b>", 2, 0);
	  this.__addInfo("No sessions highlighted.", 2, 1);
      this.__addLabel("<b>Scan Date :</b>", 3, 0);
      this.__addLabel("<b>Upload Date :</b>", 4, 0);
      this.__addLabel("<b>Status :</b>", 5, 0);
      this.__addLabel("<b>Scanner :</b>", 6, 0);
	  this.__addLabel("<b>More :</b>", 7, 0);
    },

    __applySessionShowing : function (newData, oldData) {
    },
    __showNewSessDetails : function (data) {
    },
    /**
     * Create the details box.
     */
    create : function () {
      this.__sessionDetailsStack = new qx.ui.container.Stack(new qx.ui.layout.VBox());
      this.__sessionDetailsStack.setMinWidth(315);
      this.__blocker = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
      this.__blocker.setBackgroundColor("yellow");

      this.__sessionDetailsContainer = new qx.ui.groupbox.GroupBox("Details of Highlighted Session(s)");
      this.__sessionDetailsContainer.setLayout(new qx.ui.layout.Grid(8,8));

      this.__initSessionDetails();
      this.__sessionDetailsStack.add(this.__sessionDetailsContainer);
      this.__sessionDetailsStack.add(this.__blocker);

      return this.__sessionDetailsStack;
    },
	 /**
     * Update the details with the given session data
     * @param data{Object} Given session.
     */
    onUpdate : function (data) {
      this.__addInfo(data.project, 0, 1);
	  this.__addInfo(data.subject, 1, 1);
      this.__addInfo(data.name, 2, 1);
      this.__addInfo(data.scan_date, 3, 1);
      this.__addInfo(data.uploaded, 4, 1);
	  this.__addInfo(this.__translateStatus(data.status), 5,1);
	  this.__addInfo(data.scanner, 6,1);
	  if(this.__sessionDetailsContainer.getLayout().getCellWidget(7,1)!=null){
		this.__sessionDetailsContainer.getLayout().getCellWidget(7,1).destroy();
	  }
	  var statusStuff = new qx.ui.container.Composite(new qx.ui.layout.HBox(7,"left"));
	  this.__sessionDetailsContainer.getLayout().getCellWidget(7,0).setMarginTop(5);
	  var moreDetails = new qx.ui.form.Button("Details");
	  
	  //Only enable the button if the session is not in receiving state.
	  if(data.status!="RECEIVING" && data.status!="_RECEIVING"){
		moreDetails.addListener("execute", function (e) {
			   window.open(serverRoot+"/data" + data.url + "?format=html&screen=PrearchiveDetails.vm&popup=false");
			 },this);
	  }
	  else{
		moreDetails.setEnabled(false);
	  }
	  
	  statusStuff.add(moreDetails);
	  this.__sessionDetailsContainer.add(statusStuff, {row : 7, column : 1});
      this.__unique_id = data.unique_id;
      this.debug(data.url);
    },
	 /**
     * Update the details with the given session data
     * @param data{Object} Given session.
     */
    onUpdateMultiple : function (data,numSessions) {
      this.__addInfo("", 0, 1);
	  this.__addInfo("", 1, 1);
      this.__addInfo(numSessions+" sessions highlighted.", 2, 1);
      this.__addInfo("", 3, 1);
      this.__addInfo("", 4, 1);
	  this.__addInfo("", 5,1);
	  this.__addInfo("", 6,1);
	  if(this.__sessionDetailsContainer.getLayout().getCellWidget(7,1)!=null){
		this.__sessionDetailsContainer.getLayout().getCellWidget(7,1).destroy();
	  }
      this.__addInfo("", 7,1);
      this.__unique_id = data.unique_id;
      this.debug(data.url);
    },
    /**
     * Clear the details box.
     */
    clear : function () {
      this.__addInfo("", 0, 1);
      this.__addInfo("", 1, 1);
      this.__addInfo("No sessions highlighted.", 2, 1);
      this.__addInfo("", 3, 1);
      this.__addInfo("", 4, 1);
	  this.__addInfo("", 5, 1);
	  this.__addInfo("", 6, 1);
      if(this.__sessionDetailsContainer.getLayout().getCellWidget(7,1)!=null){
		this.__sessionDetailsContainer.getLayout().getCellWidget(7,1).destroy();
	  }
      this.__addInfo("", 7, 1);
      this.__unique_id = -1;
    },
    /**
     * Clear the details box if given index matches the session being displayed.
     * @param index{Int} Unique session id
     */
    remove : function (index) {
      if (this.__unique_id == index) {
	this.clear();
	this.__unique_id = null;
      }
    }
  }
});
