/**
 * Manage two qx.ui.form.Datefield widgets that represent a range of dates.
 */
qx.Class.define("prearchiveui.DateRangeView",
{
  extend : qx.core.Object,
  /**
   * @param model{prearchiveui.DateRangeModel} Model which controls this view.
   */
  construct : function (model) {
    this.base(arguments);
    this.initModel(model);
    this.__init();
  },
  properties : {
    /**
     * The textfield showing the beginning of the date range.
     */
    first : {
      check : "qx.ui.form.DateField",
      deferredInit : true
    },
    /**
     * The textfield showing end of the date range.
     */
    second : {
      check : "qx.ui.form.DateField",
      deferredInit : true
    },
    /**
     * A validator for the beginning date
     */
    firstManager : {
      check : "qx.ui.form.validation.Manager",
      deferredInit : true
    },
    /**
     * A validator for the end date
     */
    secondManager : {
      check : "qx.ui.form.validation.Manager",
      deferredInit : true
    },
   /**
    * See constructor comment for explanation on this property.
    */
    model : {
      check : "prearchiveui.DateRangeModel",
      deferredInit : true
    }
  },
  members : {
    __init : function () {
      this.initFirst(new qx.ui.form.DateField());
      this.getFirst().setPlaceholder("From ...");
      this.initSecond(new qx.ui.form.DateField());
      this.getSecond().setPlaceholder("To ...");
      this.initFirstManager(new qx.ui.form.validation.Manager());
      this.initSecondManager(new qx.ui.form.validation.Manager());
      this.getFirstManager().add(this.getFirst(), this.getModel().firstValidator, this.getModel());
      this.getSecondManager().add(this.getSecond(), this.getModel().secondValidator, this.getModel());
      this.getFirst().addListener("changeValue", function (e) {
				    this.getFirstManager().validate();
				  }, this);
      this.getSecond().addListener("changeValue", function (e) {
				    this.getSecondManager().validate();
				   },this);
    },
    /**
     * Clear the date range
     */
    clear : function () {
      this.getFirst().resetValue();
      this.getSecond().resetValue();
      this.getModel().clear();
    }
  }
});
