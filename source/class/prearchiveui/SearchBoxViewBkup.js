qx.Class.define("prearchiveui.SearchBoxViewBkup",
{
  extend : qx.core.Object,
  construct : function (model) {
    this.base(arguments);
    if (model != null) {
      this.__model = model;
    }
    else {
      throw new Error ("SearchBoxView : model is null");
    }
  },
  members : {
    __model : null,
    __searchBoxContainer : null,
    __buttonPane : null,
    __makeRichLabel : function (v,buddy){
      var label = new qx.ui.basic.Label();
      label.setRich(true);
      label.setValue(v);
      if (buddy != null) {
	label.setBuddy(buddy);
      }
      return label;
    },

    __initButtonPane : function () {
      var paneLayout = new qx.ui.layout.HBox().set({
						     spacing: 4,
						     alignX : "right"
						   });

      this.__buttonPane = new qx.ui.container.Composite(paneLayout).set({
								       paddingTop: 11
								     });
//      this.__buttonPane.add(this.__createButton("Clear"));
//      this.__searchBoxContainer.add(this.__buttonPane, {row:6, column: 0, colSpan: 2});
    },
    __createButton : function (name, event, func) {
      var button = new qx.ui.form.Button(name);
      if (event != null) {
      	if (func != null) {
      	  button.addListener(event,func,this);
      	}
      }
      return button;
    },
    create : function (dataFunc,container) {
      var that = this;
      this.__searchBoxContainer = new qx.ui.groupbox.GroupBox("Filters");
      this.__searchBoxContainer.setLayout(new qx.ui.layout.Grid(8,8));

      var t = new tokenfield.Token();
      t.setSelectionMode('multi');

      t.setHintText("Type in a project name here");
      t.addListener("loadData", function(e) {
		      var str = e.getData();
		      var _data = dataFunc.projects();
		      var data = [];
		      for (var datum = 0; datum < _data.length; datum++) {
			if (qx.lang.String.contains(_data[datum], str)) {
			  data.push({label : _data[datum]});
			}
		      }
		      t.populateList(str, data);
		    },this);
      t.addListener("addItem", function(e) {
		      var item = e.getData();
		      this.__model.addProject(item.getLabel());
		    },this);
      t.addListener("removeItem", function(e) {
		      var item = e.getData();
		      this.__model.removeProject(item.getLabel());
		    },this);

      var s = new prearchiveui.RestrictedToken();
      s.setSelectionMode('multi');
      s.setHintText("Type in a subject/session name here");
      s.addListener("loadData", function(e) {
		      var str = e.getData();
		      var _data = dataFunc.subjects();
		      var data = [];
		      counter = 0;
		      for (var datum = 0; datum < _data.length; datum++) {
			var found = false;
			if (qx.lang.String.contains(_data[datum].session, str)) {
			  data[counter] = {label : _data[datum].session};
			  found = true;
			  counter++;
			}
			if (qx.lang.String.contains(_data[datum].subject, str) && !found) {
			  data[counter] = {label : _data[datum].subject};
			  found = true;
			  counter++;
			}
		      }
		      s.populateList(str, data);
		    },this);
      s.addListener("addItem", function(e) {
		      var item = e.getData();
		      this.__model.addSubjectOrSession(item.getLabel());
		    },this);
      s.addListener("removeItem", function(e) {
		      var item = e.getData();
		      this.__model.removeSubjectOrSession(item.getLabel());
		    },this);

      var uploadDateRange = new prearchiveui.DateRangeView(new prearchiveui.DateRangeModel("upload"));
      var scanDateRange = new prearchiveui.DateRangeView(new prearchiveui.DateRangeModel("scanDate"));

      this.__searchBoxContainer.add(this.__makeRichLabel("<b>Project : </b>", this.__textfield),
	                            {row : 0, column : 0});
      this.__searchBoxContainer.add(t,
	                            {row : 0, column : 1});
      this.__searchBoxContainer.add(this.__makeRichLabel("<b>Upload Date : </b>", this.__textfield),
	                            {row : 1, column : 0});
      this.__searchBoxContainer.add(uploadDateRange.getFirst(),
	                            {row : 1, column : 1});
      this.__searchBoxContainer.add(uploadDateRange.getSecond(),
	                            {row : 2, column : 1});
      this.__searchBoxContainer.add(this.__makeRichLabel("<b>Session Date : </b>", this.__textfield),
	                            {row : 3, column : 0});
      this.__searchBoxContainer.add(scanDateRange.getFirst(),
	                            {row : 3, column : 1});
      this.__searchBoxContainer.add(scanDateRange.getSecond(),
	                            {row : 4, column : 1});
      this.__searchBoxContainer.add(this.__makeRichLabel("<b>Subject/Session : </b>", this.__textfield),
	                            {row : 5, column : 0});
      this.__searchBoxContainer.add(s,
	                            {row : 5, column : 1});

      // this.__searchBoxContainer.add(button,                                                              {row : 6, column : 0});

      this.__initButtonPane();
      container.add(this.__searchBoxContainer);
    }
  }
});
      // var uploadDateFirst = new qx.ui.form.DateField();
      // var uploadDateLast = new qx.ui.form.DateField();

      // var sessionDateFirst = new qx.ui.form.DateField();
      // var sessionDateLast = new qx.ui.form.DateField();

      // var uploadDateManager = new qx.ui.form.validation.Manager();
      // var sessionDateManager = new qx.ui.form.validation.Manager();

      // var uploadDateRange =  new prearchiveui.DateRange();
      // var sessionDateRange = new prearchiveui.DateRange();


      // var firstUploadDateValidator = function (value,item) {
      // 	var valid = true;
      // 	  try {
      // 	    that.__model.addUploadDate(true, value);
      // 	  }
      // 	  catch (ex) {
      // 	    item.setInvalidMessage(ex.toString());
      // 	    valid = false;
      // 	  }
      // 	return valid;
      // };

      // var secondUploadDateValidator = function (value,item) {
      // 	var valid = true;
      // 	  try {
      // 	    that.__model.addUploadDate(false, value);
      // 	  }
      // 	  catch (ex) {
      // 	    item.setInvalidMessage(ex.toString());
      // 	    valid = false;
      // 	  }
      // 	return valid;
      // };

      // var firstSessionDateValidator = function (value,item) {
      // 	var valid = true;
      // 	  try {
      // 	    that.__model.addSessionDate(true, value);
      // 	  }
      // 	  catch (ex) {
      // 	    item.setInvalidMessage(ex.toString());
      // 	    valid = false;
      // 	}
      // 	return valid;
      // };

      // var secondSessionDateValidator = function (value,item) {
      // 	var valid = true;
      // 	  try {
      // 	    that.__model.addSessionDate(false, value);
      // 	  }
      // 	  catch (ex) {
      // 	    item.setInvalidMessage(ex.toString());
      // 	    valid = false;
      // 	}
      // 	return valid;
      // };


      // uploadDateManager.add(uploadDateFirst, firstUploadDateValidator);
      // uploadDateManager.add(uploadDateLast,  secondUploadDateValidator);

      // sessionDateManager.add(sessionDateFirst, firstSessionDateValidator);
      // sessionDateManager.add(sessionDateLast,  secondSessionDateValidator);


      // uploadDateFirst.addListener("changeValue", function (e) {
      // 				    uploadDateManager.validate();
      // 				  }, this);

      // uploadDateLast.addListener("changeValue", function (e) {
      // 				   uploadDateManager.validate();
      // 				  }, this);

      // sessionDateFirst.addListener("changeValue", function (e) {
      // 				    sessionDateManager.validate();
      // 				  }, this);

      // sessionDateLast.addListener("changeValue", function (e) {
      // 				   sessionDateManager.validate();
      // 				  }, this);

