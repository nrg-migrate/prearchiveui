/**
 * <pre>
 * The class manages two qx.ui.form.Datefield widgets that represent a date range. This range
 * would then be used to filter through the sessions to find ones that fall within that range.
 *
 * It signals an error if the first date is greater than the second and vice versa.
 *
 * UGLY HACK ALERT!
 * Based on requirements:
 * - if the first date field was empty, the filter should show all sessions upto the second date and,
 * - if the second date field was empty, the filter should show all session from the first date.
 * In order to do this the first field is hard-coded with "Dec 31 00:00:00 CST 1800" and the second
 * with "Dec 31 00:00:00 CST 3000".
 *
 * TODO : Fix the ugly hack described above.
 * </pre>
 *
 */
qx.Class.define("prearchiveui.DateRangeModel",
{
  extend : qx.core.Object,
  construct : function () {
    this.base(arguments);
    this.__defaultFirstDate = new Date("Dec 31 00:00:00 CST 1800");
    this.__defaultSecondDate = new Date("Dec 31 00:00:00 CST 3000");
    this.initFirst(this.__defaultFirstDate);
    this.initSecond(this.__defaultSecondDate);
    this.initView(new prearchiveui.DateRangeView(this));
  },
  events : {
    /**
     * See prearchiveui.SessionController for explanation on this event
     */
    "changeDateRange" : "qx.event.type.Data"
  },
  properties : {
    /**
     * The beginning of the date range
     */
    first : {
      check : "Date",
      apply : "_applyFirst",
      deferredInit : true,
      nullable : true
    },
    /**
     * The end of the date range.
     */
    second : {
      check : "Date",
      apply : "_applySecond",
      deferredInit : true,
      nullable : true
    },
    /**
     * The view that displays the date input textfields
     */
    view : {
      deferredInit : true
    }
  },
  members : {
    __first : true,
    /**
     * Validate the first date
     * @param value{String} The value to validate
     * @param item{Object} The widget to flag if the value is invalid
     */
    firstValidator : function (value, item) {
		if(value==null){
		this.setFirst(new Date("Dec 31 00:00:00 CST 1800"));
	throw new qx.core.ValidationError
          ( "Invalid Date. Dates must be formatted like \"Jan 1, 2013\".");
	}
      var valid = true;
      var that = this;
      try {
	this.setFirst(value);
      }
      catch (ex) {
	item.setInvalidMessage(ex.toString());
	valid = false;
      }
      return valid;
    },
    /**
     * Validate the second date
     * @param value{String} The value to validate
     * @param item{Object} The widget to flag if the value is invalid
     */
    secondValidator : function (value, item) {
	if(value==null){
	this.setSecond(new Date("Dec 31 00:00:00 CST 3000"));
	throw new qx.core.ValidationError
          ( "Invalid Date. Dates must be formatted like \"Jan 1, 2013\".");
	}
      var valid = true;
      try {
	this.setSecond(value);
      }
      catch (ex) {
	item.setInvalidMessage(ex.toString());
	valid = false;
      }
      return valid;
    },
    _applyFirst : function (value, old) {
      if (this.__first) {
	this.__first = false;
	return;
      }
      if (value == null) {
	this.resetFirst();
      }
      else {
	if (value > this.getSecond()){
	  throw new qx.core.ValidationError
          ( "First date must be less than the second date.");
	}
	else {
	  this.fireDataEvent("changeDateRange", this);
	}
      }
    },

    _applySecond : function (value, old) {
      if (this.__first) {
	    this.__first = false;
	    return;
      }
      if (value == null) {
	    this.resetSecond();
      }
      else {
	    if (value < this.getFirst()){
	      throw new qx.core.ValidationError( "Second date must be greater than the first date");
	    }
	    else {
	      this.fireDataEvent("changeDateRange", this);
	    }
      }
    },

    /**
     * Clear the date range
     */
    clear : function () {
      this.resetFirst();
      this.resetSecond();
    },
    /**
     * Check if there are user populated values in this date range
     */
    isActive : function () {
      return !((this.getFirst() == this.__defaultFirstDate)
	       &&
	       (this.getSecond() == this.__defaultSecondDate));
    }
  }
});
